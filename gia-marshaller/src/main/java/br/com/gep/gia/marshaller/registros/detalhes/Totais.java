package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class Totais {

    @Field(at = 734, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo01;

    @Field(at = 737, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo01C;

    @Field(at = 740, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo02;

    @Field(at = 743, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo03;

    @Field(at = 746, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo04;

    @Field(at = 749, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo05;

    @Field(at = 752, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo05A;

    @Field(at = 755, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo05B;

    @Field(at = 758, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo05C;

    @Field(at = 761, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo06;

    @Field(at = 764, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo07;

    @Field(at = 767, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo07A;

    @Field(at = 770, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo07B;

    @Field(at = 773, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo08;

    @Field(at = 776, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo09;

    @Field(at = 779, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo10;

    @Field(at = 782, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo14;

    @Field(at = 785, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo15;

    @Field(at = 788, length = 3, align = Align.RIGHT, padding = '0')
    private int qtdeLinhasAnexo16;

    @Field(at = 791, length = 1)
    private boolean sistemaProprio = true;

    @Field(at = 792, length = 1)
    private boolean sintegra = false;

    @Field(at = 793, length = 1)
    private boolean inicioAtividade = false;

    @Field(at = 794, length = 1)
    private boolean fimAtividade = false;

    public int getQtdeLinhasAnexo01() {
        return qtdeLinhasAnexo01;
    }

    public void setQtdeLinhasAnexo01(int qtdeLinhasAnexo01) {
        this.qtdeLinhasAnexo01 = qtdeLinhasAnexo01;
    }

    public int getQtdeLinhasAnexo01C() {
        return qtdeLinhasAnexo01C;
    }

    public void setQtdeLinhasAnexo01C(int qtdeLinhasAnexo01C) {
        this.qtdeLinhasAnexo01C = qtdeLinhasAnexo01C;
    }

    public int getQtdeLinhasAnexo02() {
        return qtdeLinhasAnexo02;
    }

    public void setQtdeLinhasAnexo02(int qtdeLinhasAnexo02) {
        this.qtdeLinhasAnexo02 = qtdeLinhasAnexo02;
    }

    public int getQtdeLinhasAnexo03() {
        return qtdeLinhasAnexo03;
    }

    public void setQtdeLinhasAnexo03(int qtdeLinhasAnexo03) {
        this.qtdeLinhasAnexo03 = qtdeLinhasAnexo03;
    }

    public int getQtdeLinhasAnexo04() {
        return qtdeLinhasAnexo04;
    }

    public void setQtdeLinhasAnexo04(int qtdeLinhasAnexo04) {
        this.qtdeLinhasAnexo04 = qtdeLinhasAnexo04;
    }

    public int getQtdeLinhasAnexo05() {
        return qtdeLinhasAnexo05;
    }

    public void setQtdeLinhasAnexo05(int qtdeLinhasAnexo05) {
        this.qtdeLinhasAnexo05 = qtdeLinhasAnexo05;
    }

    public int getQtdeLinhasAnexo05A() {
        return qtdeLinhasAnexo05A;
    }

    public void setQtdeLinhasAnexo05A(int qtdeLinhasAnexo05A) {
        this.qtdeLinhasAnexo05A = qtdeLinhasAnexo05A;
    }

    public int getQtdeLinhasAnexo05B() {
        return qtdeLinhasAnexo05B;
    }

    public void setQtdeLinhasAnexo05B(int qtdeLinhasAnexo05B) {
        this.qtdeLinhasAnexo05B = qtdeLinhasAnexo05B;
    }

    public int getQtdeLinhasAnexo05C() {
        return qtdeLinhasAnexo05C;
    }

    public void setQtdeLinhasAnexo05C(int qtdeLinhasAnexo05C) {
        this.qtdeLinhasAnexo05C = qtdeLinhasAnexo05C;
    }

    public int getQtdeLinhasAnexo06() {
        return qtdeLinhasAnexo06;
    }

    public void setQtdeLinhasAnexo06(int qtdeLinhasAnexo06) {
        this.qtdeLinhasAnexo06 = qtdeLinhasAnexo06;
    }

    public int getQtdeLinhasAnexo07() {
        return qtdeLinhasAnexo07;
    }

    public void setQtdeLinhasAnexo07(int qtdeLinhasAnexo07) {
        this.qtdeLinhasAnexo07 = qtdeLinhasAnexo07;
    }

    public int getQtdeLinhasAnexo07A() {
        return qtdeLinhasAnexo07A;
    }

    public void setQtdeLinhasAnexo07A(int qtdeLinhasAnexo07A) {
        this.qtdeLinhasAnexo07A = qtdeLinhasAnexo07A;
    }

    public int getQtdeLinhasAnexo07B() {
        return qtdeLinhasAnexo07B;
    }

    public void setQtdeLinhasAnexo07B(int qtdeLinhasAnexo07B) {
        this.qtdeLinhasAnexo07B = qtdeLinhasAnexo07B;
    }

    public int getQtdeLinhasAnexo08() {
        return qtdeLinhasAnexo08;
    }

    public void setQtdeLinhasAnexo08(int qtdeLinhasAnexo08) {
        this.qtdeLinhasAnexo08 = qtdeLinhasAnexo08;
    }

    public int getQtdeLinhasAnexo09() {
        return qtdeLinhasAnexo09;
    }

    public void setQtdeLinhasAnexo09(int qtdeLinhasAnexo09) {
        this.qtdeLinhasAnexo09 = qtdeLinhasAnexo09;
    }

    public int getQtdeLinhasAnexo10() {
        return qtdeLinhasAnexo10;
    }

    public void setQtdeLinhasAnexo10(int qtdeLinhasAnexo10) {
        this.qtdeLinhasAnexo10 = qtdeLinhasAnexo10;
    }

    public int getQtdeLinhasAnexo14() {
        return qtdeLinhasAnexo14;
    }

    public void setQtdeLinhasAnexo14(int qtdeLinhasAnexo14) {
        this.qtdeLinhasAnexo14 = qtdeLinhasAnexo14;
    }

    public int getQtdeLinhasAnexo15() {
        return qtdeLinhasAnexo15;
    }

    public void setQtdeLinhasAnexo15(int qtdeLinhasAnexo15) {
        this.qtdeLinhasAnexo15 = qtdeLinhasAnexo15;
    }

    public int getQtdeLinhasAnexo16() {
        return qtdeLinhasAnexo16;
    }

    public void setQtdeLinhasAnexo16(int qtdeLinhasAnexo16) {
        this.qtdeLinhasAnexo16 = qtdeLinhasAnexo16;
    }

    public boolean isSistemaProprio() {
        return sistemaProprio;
    }

    public void setSistemaProprio(boolean sistemaProprio) {
        this.sistemaProprio = sistemaProprio;
    }

    public boolean isSintegra() {
        return sintegra;
    }

    public void setSintegra(boolean sintegra) {
        this.sintegra = sintegra;
    }

    public boolean isInicioAtividade() {
        return inicioAtividade;
    }

    public void setInicioAtividade(boolean inicioAtividade) {
        this.inicioAtividade = inicioAtividade;
    }

    public boolean isFimAtividade() {
        return fimAtividade;
    }

    public void setFimAtividade(boolean fimAtividade) {
        this.fimAtividade = fimAtividade;
    }
}
