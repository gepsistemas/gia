package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;

/**
 * Outras Saídas – Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X07 ")
})
public class Anexo07 extends Anexo<Anexo07> {

    @Field(at = 36, length = 13, handlerName = "double13")
    private Double creditoEntradasSt = 0D; // anexo VIIa

    @Field(at = 49, length = 13, handlerName = "double13")
    private Double outrosCreditos = 0D;

    @Field(at = 62, length = 60)
    private String textoOutrosCreditos;

    @Field(at = 122, length = 13, handlerName = "double13")
    private double totalCreditos;

    @Field(at = 135, length = 13, handlerName = "double13")
    private Double debitoSaidasSt = 0D; // anexo VIIb

    @Field(at = 148, length = 13, handlerName = "double13")
    private Double outrosDebitos = 0D;

    @Field(at = 161, length = 60)
    private String textoOutrosDebitos;

    @Field(at = 221, length = 13, handlerName = "double13")
    private double totalDebitos;

    public Anexo07() {
        ocorrencias = 1;
    }

    public double getCreditoEntradasSt() {
        return creditoEntradasSt;
    }

    public void setCreditoEntradasSt(Double creditoEntradasSt) {
        if (creditoEntradasSt == null) creditoEntradasSt = 0D;
        totalCreditos -= this.creditoEntradasSt;
        totalCreditos += creditoEntradasSt;
        this.creditoEntradasSt = creditoEntradasSt;
    }

    public double getOutrosCreditos() {
        return outrosCreditos;
    }

    public void setOutrosCreditos(Double outrosCreditos) {
        if (outrosCreditos == null) outrosCreditos = 0D;
        totalCreditos -= this.outrosCreditos;
        totalCreditos += outrosCreditos;
        this.outrosCreditos = outrosCreditos;
    }

    public String getTextoOutrosCreditos() {
        return textoOutrosCreditos;
    }

    public void setTextoOutrosCreditos(String textoOutrosCreditos) {
        this.textoOutrosCreditos = textoOutrosCreditos;
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    public double getDebitoSaidasSt() {
        return debitoSaidasSt;
    }

    public void setDebitoSaidasSt(Double debitoSaidasSt) {
        if (debitoSaidasSt == null) debitoSaidasSt = 0D;
        totalDebitos -= this.debitoSaidasSt;
        totalDebitos += debitoSaidasSt;
        this.debitoSaidasSt = debitoSaidasSt;
    }

    public double getOutrosDebitos() {
        return outrosDebitos;
    }

    public void setOutrosDebitos(Double outrosDebitos) {
        if (outrosDebitos == null) outrosDebitos = 0D;
        totalDebitos -= this.outrosDebitos;
        totalDebitos += outrosDebitos;
        this.outrosDebitos = outrosDebitos;
    }

    public String getTextoOutrosDebitos() {
        return textoOutrosDebitos;
    }

    public void setTextoOutrosDebitos(String textoOutrosDebitos) {
        this.textoOutrosDebitos = textoOutrosDebitos;
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }
}
