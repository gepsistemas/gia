package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

import java.util.Date;

public class DetalheAnexo08e10 {

    @Field(at = 36, length = 2, align = Align.RIGHT, padding = '0')
    private int diaInicioApuracao;

    @Field(at = 38, length = 2, align = Align.RIGHT, padding = '0')
    private int diaTerminoApuracao;

    @Field(at = 40, length = 8)
    private Date vencimento;

    @Field(at = 48, length = 13, handlerName = "double13")
    private double icmsProprio;

    @Field(at = 61, length = 13, handlerName = "double13")
    private double icmsSt;

    @Field(at = 74, length = 10, align = Align.RIGHT, padding = '0')
    private String ie = "";

    public DetalheAnexo08e10() {
    }

    public DetalheAnexo08e10(int diaInicioApuracao, int diaTerminoApuracao, Date vencimento, double icmsProprio, double icmsSt, String ie) {
        this.diaInicioApuracao = diaInicioApuracao;
        this.diaTerminoApuracao = diaTerminoApuracao;
        this.vencimento = vencimento;
        this.icmsProprio = icmsProprio;
        this.icmsSt = icmsSt;
       setIe(ie);
    }

    public int getDiaInicioApuracao() {
        return diaInicioApuracao;
    }

    public void setDiaInicioApuracao(int diaInicioApuracao) {
        this.diaInicioApuracao = diaInicioApuracao;
    }

    public int getDiaTerminoApuracao() {
        return diaTerminoApuracao;
    }

    public void setDiaTerminoApuracao(int diaTerminoApuracao) {
        this.diaTerminoApuracao = diaTerminoApuracao;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public double getIcmsProprio() {
        return icmsProprio;
    }

    public void setIcmsProprio(double icmsProprio) {
        this.icmsProprio = icmsProprio;
    }

    public double getIcmsSt() {
        return icmsSt;
    }

    public void setIcmsSt(double icmsSt) {
        this.icmsSt = icmsSt;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie == null ? "" : ie;
    }

    public double getTotalIcms() {
        return icmsProprio + icmsSt;
    }
}
