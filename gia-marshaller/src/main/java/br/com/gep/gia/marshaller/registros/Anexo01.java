package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo01;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação das entradas
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X01 ")
})
public class Anexo01 extends AnexoComDetalhe<Anexo01, DetalheAnexo01> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo01> detalhes;

    private double totalValorContabil;
    private double totalCreditos;

    public Anexo01() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo01> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalValorContabil() {
        return totalValorContabil;
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    @Override
    public Anexo01 adicionarDetalhe(DetalheAnexo01 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalCreditos += detalhe.getCredito();
        totalValorContabil += detalhe.getValorContabil();
        return this;
    }

    @Override
    public Anexo01 removerDetalhe(DetalheAnexo01 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalCreditos -= detalhe.getCredito();
        totalValorContabil -= detalhe.getValorContabil();
        return this;
    }
}
