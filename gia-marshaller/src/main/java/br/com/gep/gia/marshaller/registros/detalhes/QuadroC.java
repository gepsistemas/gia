package br.com.gep.gia.marshaller.registros.detalhes;

import br.com.gep.gia.marshaller.registros.Anexo01;
import br.com.gep.gia.marshaller.registros.Anexo05;
import br.com.gep.gia.marshaller.registros.Anexo08;
import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class QuadroC {

    @Field(at = 419, length = 15, handlerName = "double15")
    private double faturamentoMes;

    @Field(at = 434, length = 6, align = Align.RIGHT, padding = '0')
    private int numeroEmpregados;

    @Field(at = 440, length = 15, handlerName = "double15")
    private double valorFolhaPagamento;

    @Field(at = 455, length = 15, handlerName = "double15")
    private double internaEntrada; // desativado (deve ser preenchido com zeros)

    @Field(at = 470, length = 15, handlerName = "double15")
    private double internaSaida; // desativado (deve ser preenchido com zeros)

    @Field(at = 485, length = 15, handlerName = "double15")
    private double outrosUfEntrada; // desativado (deve ser preenchido com zeros)

    @Field(at = 500, length = 15, handlerName = "double15")
    private double outrosUfSaida; // desativado (deve ser preenchido com zeros)

    @Field(at = 515, length = 15, handlerName = "double15")
    private double outrosPaisesEntrada; // desativado (deve ser preenchido com zeros)

    @Field(at = 530, length = 15, handlerName = "double15")
    private double outrosPaisesSaida; // desativado (deve ser preenchido com zeros)

    @Field(at = 545, length = 15, handlerName = "double15")
    private double totalEntradas;

    @Field(at = 560, length = 15, handlerName = "double15")
    private double totalSaidas;

    @Field(at = 575, length = 15, handlerName = "double15")
    private double pagamentoFatoGeradorIcmsProprio;

    @Field(at = 590, length = 15, handlerName = "double15")
    private double pagamentoFatoGeradorIcmsST;

    @Field(at = 605, length = 1)
    private boolean transportaSadoDevedor;

    public double getFaturamentoMes() {
        return faturamentoMes;
    }

    public void setFaturamentoMes(double faturamentoMes) {
        this.faturamentoMes = faturamentoMes;
    }

    public int getNumeroEmpregados() {
        return numeroEmpregados;
    }

    public void setNumeroEmpregados(int numeroEmpregados) {
        this.numeroEmpregados = numeroEmpregados;
    }

    public double getValorFolhaPagamento() {
        return valorFolhaPagamento;
    }

    public void setValorFolhaPagamento(double valorFolhaPagamento) {
        this.valorFolhaPagamento = valorFolhaPagamento;
    }

    public double getTotalEntradas() {
        return totalEntradas;
    }

    public void setTotalEntradas(double totalEntradas) {
        this.totalEntradas = totalEntradas;
    }

    public double getTotalSaidas() {
        return totalSaidas;
    }

    public void setTotalSaidas(double totalSaidas) {
        this.totalSaidas = totalSaidas;
    }

    public double getPagamentoFatoGeradorIcmsProprio() {
        return pagamentoFatoGeradorIcmsProprio;
    }

    public void setPagamentoFatoGeradorIcmsProprio(double pagamentoFatoGeradorIcmsProprio) {
        this.pagamentoFatoGeradorIcmsProprio = pagamentoFatoGeradorIcmsProprio;
    }

    public double getPagamentoFatoGeradorIcmsST() {
        return pagamentoFatoGeradorIcmsST;
    }

    public void setPagamentoFatoGeradorIcmsST(double pagamentoFatoGeradorIcmsST) {
        this.pagamentoFatoGeradorIcmsST = pagamentoFatoGeradorIcmsST;
    }

    public boolean isTransportaSadoDevedor() {
        return transportaSadoDevedor;
    }

    public void setTransportaSadoDevedor(boolean transportaSadoDevedor) {
        this.transportaSadoDevedor = transportaSadoDevedor;
    }

    public void aplicarRegras(Anexo01 anexo01, Anexo05 anexo05, Anexo08 anexo08) {
        totalEntradas = anexo01 != null ? anexo01.getTotalValorContabil() : 0;
        totalSaidas = anexo05 != null ? anexo05.getTotalValorContabil() : 0;
        if (anexo08 != null) {
            pagamentoFatoGeradorIcmsProprio = anexo08.getTotalIcmsProprio();
            pagamentoFatoGeradorIcmsST = anexo08.getTotalIcmsSt();
        }
    }
}
