package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo01Ce05C;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Importâncias Excluídas/Ajustes do VA – Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X01C")
})
public class Anexo01C extends AnexoComDetalhe<Anexo01C, DetalheAnexo01Ce05C> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo01Ce05C> detalhes;

    private double total;

    public Anexo01C() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo01Ce05C> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotal() {
        return total;
    }

    @Override
    public Anexo01C adicionarDetalhe(DetalheAnexo01Ce05C detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        total += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo01C removerDetalhe(DetalheAnexo01Ce05C detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        total -= detalhe.getValor();
        return this;
    }
}
