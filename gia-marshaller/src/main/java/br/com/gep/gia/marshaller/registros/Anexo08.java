package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo08e10;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Pagamentos de ICMS Efetuados no Mês de Referência
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X08 ")
})
public class Anexo08 extends AnexoComDetalhe<Anexo08, DetalheAnexo08e10> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo08e10> detalhes;

    private double totalIcmsProprio;
    private double totalIcmsSt;
    private double totalGeral;

    public Anexo08() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo08e10> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalGeral() {
        return totalGeral;
    }

    public double getTotalIcmsProprio() {
        return totalIcmsProprio;
    }

    public double getTotalIcmsSt() {
        return totalIcmsSt;
    }

    @Override
    public Anexo08 adicionarDetalhe(DetalheAnexo08e10 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalGeral += detalhe.getTotalIcms();
        totalIcmsProprio += detalhe.getIcmsProprio();
        totalIcmsSt += detalhe.getIcmsSt();
        return this;
    }

    @Override
    public Anexo08 removerDetalhe(DetalheAnexo08e10 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalGeral -= detalhe.getTotalIcms();
        totalIcmsProprio -= detalhe.getIcmsProprio();
        totalIcmsSt -= detalhe.getIcmsSt();
        return this;
    }
}
