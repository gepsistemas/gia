package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo08e10;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação ICMS a Recolher, Inclusive Saldo Devedor Acumulado
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X10 ")
})
public class Anexo10 extends AnexoComDetalhe<Anexo10, DetalheAnexo08e10> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo08e10> detalhes;

    private double totalGeral;
    private double totalIcmsProprio;

    public Anexo10() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo08e10> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalGeral() {
        return totalGeral;
    }

    @Override
    public Anexo10 adicionarDetalhe(DetalheAnexo08e10 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalGeral += detalhe.getTotalIcms();
        totalIcmsProprio += detalhe.getIcmsProprio();
        return this;
    }

    @Override
    public Anexo10 removerDetalhe(DetalheAnexo08e10 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalGeral -= detalhe.getTotalIcms();
        totalIcmsProprio -= detalhe.getIcmsProprio();
        return this;
    }
}
