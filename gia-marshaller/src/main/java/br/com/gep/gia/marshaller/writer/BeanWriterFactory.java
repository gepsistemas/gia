package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.handlers.*;
import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.tipos.Natureza;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;
import org.beanio.builder.FixedLengthParserBuilder;
import org.beanio.builder.StreamBuilder;
import org.beanio.types.DateTypeHandler;

import java.io.Writer;
import java.util.Date;

public class BeanWriterFactory {

    public static final String STREAM_NAME = "gia";

    private static volatile BeanWriterFactory instance;
    private StreamFactory streamFactory;

    private BeanWriterFactory() {}

    public static BeanWriterFactory getInstance() {
        if (instance == null) {
            synchronized (BeanWriterFactory.class) {
                if (instance == null) {
                    instance = new BeanWriterFactory()
                            .initializeStreamFactory();
                }
            }
        }

        return instance;
    }

    public BeanWriter createBeanWriter(Writer writer) {
        return streamFactory.createWriter(STREAM_NAME, writer);
    }

    public StreamFactory getStreamFactory() {
        return streamFactory;
    }

    private BeanWriterFactory initializeStreamFactory() {
        streamFactory = StreamFactory.newInstance();

        StreamBuilder builder = new StreamBuilder(STREAM_NAME)
                .format("fixedlength")
                .parser(new FixedLengthParserBuilder())
                .writeOnly()

                .addTypeHandler(Boolean.class, new SNTypeHandler())
                .addTypeHandler(Date.class, new DateTypeHandler("ddMMyyyy"))
                .addTypeHandler(Natureza.class, new NaturezaTypeHandler())
                .addTypeHandler("double13", new Double13TypeHandler())
                .addTypeHandler("double15", new Double15TypeHandler())
                .addTypeHandler("double16", new Double16TypeHandler());

        addRecords(builder);
        builder.addGroup(Estrutura.class);

        streamFactory.define(builder);

        return this;
    }

    private static void addRecords(StreamBuilder builder) {
        builder
                .addRecord(Principal.class)
                .addRecord(Observacoes.class)
                .addRecord(Contribuinte.class)
                .addRecord(Anexo01.class)
                .addRecord(Anexo01C.class)
                .addRecord(Anexo02.class)
                .addRecord(Anexo03.class)
                .addRecord(Anexo04.class)
                .addRecord(Anexo05.class)
                .addRecord(Anexo05A.class)
                .addRecord(Anexo05B.class)
                .addRecord(Anexo05C.class)
                .addRecord(Anexo06.class)
                .addRecord(Anexo07.class)
                .addRecord(Anexo07A.class)
                .addRecord(Anexo07B.class)
                .addRecord(Anexo08.class)
                .addRecord(Anexo09.class)
                .addRecord(Anexo10.class)
                .addRecord(Anexo14.class)
                .addRecord(Anexo15.class)
                .addRecord(Anexo16.class);
    }
}
