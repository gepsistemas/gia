package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo14e15 {

    @Field(at = 36, length = 3, align = Align.RIGHT, padding = '0')
    private int codigo;

    @Field(at = 39, length = 13, handlerName = "double13")
    private double valor;

    @Field(at = 52, length = 60)
    private String especificacao;

    public DetalheAnexo14e15() {
    }

    public DetalheAnexo14e15(int codigo, double valor, String especificacao) {
        this.codigo = codigo;
        this.valor = valor;
        this.especificacao = especificacao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getEspecificacao() {
        return especificacao;
    }

    public void setEspecificacao(String especificacao) {
        this.especificacao = especificacao;
    }
}
