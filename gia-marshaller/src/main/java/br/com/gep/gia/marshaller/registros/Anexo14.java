package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo14e15;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Outros Créditos - Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X14 ")
})
public class Anexo14 extends AnexoComDetalhe<Anexo14, DetalheAnexo14e15> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo14e15> detalhes;

    private double totalCreditos;

    public Anexo14() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo14e15> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    @Override
    public Anexo14 adicionarDetalhe(DetalheAnexo14e15 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalCreditos += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo14 removerDetalhe(DetalheAnexo14e15 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalCreditos -= detalhe.getValor();
        return this;
    }
}
