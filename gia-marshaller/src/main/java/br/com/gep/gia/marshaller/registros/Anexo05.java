package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo05;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação das Saídas
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X05 ")
})
public class Anexo05 extends AnexoComDetalhe<Anexo05, DetalheAnexo05> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo05> detalhes;

    private double totalValorContabil;
    private double totalDebitos;

    public Anexo05() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo05> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalValorContabil() {
        return totalValorContabil;
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }

    @Override
    public Anexo05 adicionarDetalhe(DetalheAnexo05 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalDebitos += detalhe.getDebito();
        totalValorContabil += detalhe.getValorContabil();
        return this;
    }

    @Override
    public Anexo05 removerDetalhe(DetalheAnexo05 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalDebitos -= detalhe.getDebito();
        totalValorContabil -= detalhe.getValorContabil();
        return this;
    }
}
