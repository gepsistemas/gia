package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo04;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Créditos por Compensação por Pagamentos Indevidos
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X04 ")
})
public class Anexo04 extends AnexoComDetalhe<Anexo04, DetalheAnexo04> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo04> detalhes;

    private double totalCreditos;

    public Anexo04() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo04> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    @Override
    public Anexo04 adicionarDetalhe(DetalheAnexo04 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalCreditos += detalhe.getDiferenca();
        return this;
    }

    @Override
    public Anexo04 removerDetalhe(DetalheAnexo04 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalCreditos -= detalhe.getDiferenca();
        return this;
    }
}
