package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo07AB {

    @Field(at = 36, length = 4, align = Align.RIGHT, padding = '0')
    private String cfop;

    @Field(at = 40, length = 13, handlerName = "double13")
    private double baseCalculo;

    @Field(at = 53, length = 13, handlerName = "double13")
    private double valor;

    public DetalheAnexo07AB() {
    }

    public DetalheAnexo07AB(String cfop, double baseCalculo, double valor) {
        this.cfop = cfop;
        this.baseCalculo = baseCalculo;
        this.valor = valor;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public double getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(double baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
