package br.com.gep.gia.marshaller.registros;

public abstract class AnexoComDetalhe<TAnexo extends Registro, TDetalhe> extends Anexo<TAnexo> {

    public abstract TAnexo adicionarDetalhe(TDetalhe detalhe);
    public abstract TAnexo removerDetalhe(TDetalhe detalhe);
}
