package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo03 {

    @Field(at = 36, length = 3, align = Align.RIGHT, padding = '0')
    private String codigo;

    @Field(at = 39, length = 13, handlerName = "double13")
    private double valorCredito;

    @Field(at = 52, length = 12, align = Align.RIGHT, padding = '0')
    private int chp;

    public DetalheAnexo03() {
    }

    public DetalheAnexo03(String codigo, double valorCredito, int chp) {
        this.codigo = codigo;
        this.valorCredito = valorCredito;
        this.chp = chp;
    }

    public DetalheAnexo03(String codigo, double valorCredito) {
        this.codigo = codigo;
        this.valorCredito = valorCredito;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(double valorCredito) {
        this.valorCredito = valorCredito;
    }

    public int getChp() {
        return chp;
    }

    public void setChp(int chp) {
        this.chp = chp;
    }
}
