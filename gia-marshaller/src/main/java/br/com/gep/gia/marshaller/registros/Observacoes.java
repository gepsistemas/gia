package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.builder.Align;

@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "OBS ")
})
public class Observacoes extends Registro {

    @Field(at = 34, length = 2, align = Align.RIGHT, padding = '0')
    private int ocorrencias;

    @Field(at = 36, length = 4000)
    private String texto;

    public int getOcorrencias() {
        return ocorrencias;
    }

    void setOcorrencias(int ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
        contarOcorrencias();
    }

    private void contarOcorrencias() {
        if (texto != null) {
            int tamanho = texto.length();
            ocorrencias = tamanho / 50 + (tamanho % 50 == 0 ? 0 : 1);
        }
        else ocorrencias = 0;
    }
}
