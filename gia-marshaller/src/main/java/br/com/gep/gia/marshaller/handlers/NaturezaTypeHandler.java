package br.com.gep.gia.marshaller.handlers;

import br.com.gep.gia.marshaller.tipos.Natureza;
import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

public class NaturezaTypeHandler implements TypeHandler {

    @Override
    public Object parse(String s) throws TypeConversionException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String format(Object o) {
        if (o == null) return null;

        return ((Natureza)o).getCodigo();
    }

    @Override
    public Class<?> getType() {
        return Natureza.class;
    }
}
