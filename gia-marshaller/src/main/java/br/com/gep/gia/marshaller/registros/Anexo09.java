package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo09;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação dos Débitos vencidos na Ocorrência do Fato Gerador e não Pagos
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X09 ")
})
public class Anexo09 extends AnexoComDetalhe<Anexo09, DetalheAnexo09> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo09> detalhes;

    private double totalGeral;
    private double totalIcmsProprio;
    private double totalIcmsSt;

    public Anexo09() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo09> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalGeral() {
        return totalGeral;
    }

    public double getTotalIcmsProprio() {
        return totalIcmsProprio;
    }

    public double getTotalIcmsSt() {
        return totalIcmsSt;
    }

    @Override
    public Anexo09 adicionarDetalhe(DetalheAnexo09 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalIcmsProprio += detalhe.getIcmsProprio();
        totalIcmsSt += detalhe.getIcmsSt();
        totalGeral += detalhe.getTotalIcms();
        return this;
    }

    @Override
    public Anexo09 removerDetalhe(DetalheAnexo09 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalIcmsProprio -= detalhe.getIcmsProprio();
        totalIcmsSt -= detalhe.getIcmsSt();
        totalGeral -= detalhe.getTotalIcms();
        return this;
    }
}
