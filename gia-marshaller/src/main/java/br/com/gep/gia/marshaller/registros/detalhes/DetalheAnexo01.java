package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo01 {

    @Field(at = 36, length = 4, align = Align.RIGHT, padding = '0')
    private String cfop;

    @Field(at = 40, length = 13, handlerName = "double13")
    private double valorContabil;

    @Field(at = 53, length = 13, handlerName = "double13")
    private double baseCalculo;

    @Field(at = 66, length = 13, handlerName = "double13")
    private double credito;

    @Field(at = 79, length = 13, handlerName = "double13")
    private double isentas;

    @Field(at = 92, length = 13, handlerName = "double13")
    private double outras;

    @Field(at = 105, length = 16, handlerName = "double16")
    private double excluidas;

    public DetalheAnexo01() {
    }

    public DetalheAnexo01(String cfop, double valorContabil, double baseCalculo, double credito, double isentas, double outras, double excluidas) {
        this.cfop = cfop;
        this.valorContabil = valorContabil;
        this.baseCalculo = baseCalculo;
        this.credito = credito;
        this.isentas = isentas;
        this.outras = outras;
        this.excluidas = excluidas;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public double getValorContabil() {
        return valorContabil;
    }

    public void setValorContabil(double valorContabil) {
        this.valorContabil = valorContabil;
    }

    public double getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(double baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    public double getCredito() {
        return credito;
    }

    public void setCredito(double credito) {
        this.credito = credito;
    }

    public double getIsentas() {
        return isentas;
    }

    public void setIsentas(double isentas) {
        this.isentas = isentas;
    }

    public double getOutras() {
        return outras;
    }

    public void setOutras(double outras) {
        this.outras = outras;
    }

    public double getExcluidas() {
        return excluidas;
    }

    public void setExcluidas(double excluidas) {
        this.excluidas = excluidas;
    }
}
