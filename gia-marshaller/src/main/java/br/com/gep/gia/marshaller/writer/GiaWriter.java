package br.com.gep.gia.marshaller.writer;

import org.beanio.BeanWriter;

import java.io.*;

public class GiaWriter implements Closeable {

    private BeanWriter beanWriter;

    public GiaWriter(Writer writer) {
        this.beanWriter = BeanWriterFactory.getInstance().createBeanWriter(writer);
    }

    public GiaWriter(File file) throws IOException {
        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"));
        beanWriter = BeanWriterFactory.getInstance().createBeanWriter(fw);
    }

    public void write(Object obj) throws IOException {
        beanWriter.write(obj);
    }

    public void writeAndFlush(Object obj) throws IOException {
        write(obj);
        flush();
    }

    public void flush() throws IOException {
        beanWriter.flush();
    }

    @Override
    public void close() throws IOException {
        if (beanWriter != null) {
            beanWriter.close();
            beanWriter = null;
        }
    }
}
