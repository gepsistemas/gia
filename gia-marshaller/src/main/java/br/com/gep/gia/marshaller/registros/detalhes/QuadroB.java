package br.com.gep.gia.marshaller.registros.detalhes;

import br.com.gep.gia.marshaller.registros.Anexo07;
import br.com.gep.gia.marshaller.registros.Anexo08;
import br.com.gep.gia.marshaller.registros.Anexo09;
import org.beanio.annotation.Field;

public class QuadroB {

    @Field(at = 254, length = 15, handlerName = "double15")
    private double saldoCredorAnterior;

    @Field(at = 269, length = 15, handlerName = "double15")
    private double saldoDevedorAnterior;

    @Field(at = 284, length = 15, handlerName = "double15")
    private double pagamentosMes;

    @Field(at = 299, length = 15, handlerName = "double15")
    private double debitosNaoPagos;

    @Field(at = 314, length = 15, handlerName = "double15")
    private double icmsStRecolher;

    @Field(at = 329, length = 15, handlerName = "double15")
    private double icmsProprio;

    @Field(at = 344, length = 15, handlerName = "double15")
    private double totalIcmsProprio;

    @Field(at = 359, length = 15, handlerName = "double15")
    private double creditosNaoCompensaveisMesSeguinte;

    @Field(at = 374, length = 15, handlerName = "double15")
    private double saldoCredorStMesSeguinte;

    @Field(at = 389, length = 15, handlerName = "double15")
    private double saldoCredorMesSeguinte;

    @Field(at = 404, length = 15, handlerName = "double15")
    private double saldoDevedorMesSeguinte;

    public double getSaldoCredorAnterior() {
        return saldoCredorAnterior;
    }

    public void setSaldoCredorAnterior(double saldoCredorAnterior) {
        this.saldoCredorAnterior = saldoCredorAnterior;
    }

    public double getSaldoDevedorAnterior() {
        return saldoDevedorAnterior;
    }

    public void setSaldoDevedorAnterior(double saldoDevedorAnterior) {
        this.saldoDevedorAnterior = saldoDevedorAnterior;
    }

    public double getPagamentosMes() {
        return pagamentosMes;
    }

    public void setPagamentosMes(double pagamentosMes) {
        this.pagamentosMes = pagamentosMes;
    }

    public double getDebitosNaoPagos() {
        return debitosNaoPagos;
    }

    public void setDebitosNaoPagos(double debitosNaoPagos) {
        this.debitosNaoPagos = debitosNaoPagos;
    }

    public double getIcmsStRecolher() {
        return icmsStRecolher;
    }

    public void setIcmsStRecolher(double icmsStRecolher) {
        this.icmsStRecolher = icmsStRecolher;
    }

    public double getIcmsProprio() {
        return icmsProprio;
    }

    public void setIcmsProprio(double icmsProprio) {
        this.icmsProprio = icmsProprio;
    }

    public double getTotalIcmsProprio() {
        return totalIcmsProprio;
    }

    public void setTotalIcmsProprio(double totalIcmsProprio) {
        this.totalIcmsProprio = totalIcmsProprio;
    }

    public double getCreditosNaoCompensaveisMesSeguinte() {
        return creditosNaoCompensaveisMesSeguinte;
    }

    public void setCreditosNaoCompensaveisMesSeguinte(double creditosNaoCompensaveisMesSeguinte) {
        this.creditosNaoCompensaveisMesSeguinte = creditosNaoCompensaveisMesSeguinte;
    }

    public double getSaldoCredorStMesSeguinte() {
        return saldoCredorStMesSeguinte;
    }

    public void setSaldoCredorStMesSeguinte(double saldoCredorStMesSeguinte) {
        this.saldoCredorStMesSeguinte = saldoCredorStMesSeguinte;
    }

    public double getSaldoCredorMesSeguinte() {
        return saldoCredorMesSeguinte;
    }

    public void setSaldoCredorMesSeguinte(double saldoCredorMesSeguinte) {
        this.saldoCredorMesSeguinte = saldoCredorMesSeguinte;
    }

    public double getSaldoDevedorMesSeguinte() {
        return saldoDevedorMesSeguinte;
    }

    public void setSaldoDevedorMesSeguinte(double saldoDevedorMesSeguinte) {
        this.saldoDevedorMesSeguinte = saldoDevedorMesSeguinte;
    }

    public void aplicarRegras(Anexo07 anexo07, Anexo08 anexo08, Anexo09 anexo09, QuadroA quadroA) {
        setPagamentosMes(anexo08 != null ? anexo08.getTotalGeral() : 0);
        setDebitosNaoPagos(anexo09 != null ? anexo09.getTotalGeral() : 0);
        calcularIcmsStRecolher(anexo07, anexo08, anexo09);
        calcularIcmsProprio(anexo08, anexo09, quadroA);

    }

    private void calcularIcmsStRecolher(Anexo07 anexo07, Anexo08 anexo08, Anexo09 anexo09) {
        double debitosAnexo07 = anexo07 != null ? anexo07.getTotalDebitos() : 0;
        double creditosAnexo07 = anexo07 != null ? anexo07.getTotalCreditos() : 0;
        double icmsStAnexo08 = anexo08 != null ? anexo08.getTotalIcmsSt() : 0;
        double icmsStAnexo09 = anexo09 != null ? anexo09.getTotalIcmsSt() : 0;

        icmsStRecolher = debitosAnexo07 - icmsStAnexo09 - creditosAnexo07 - icmsStAnexo08;

        if (icmsStRecolher < 0) {
            saldoCredorStMesSeguinte = Math.abs(icmsStRecolher);
            icmsStRecolher = 0;
        } else {
            saldoCredorStMesSeguinte = 0;
        }
    }

    private void calcularIcmsProprio(Anexo08 anexo08, Anexo09 anexo09, QuadroA quadroA) {
        double totalDebitos = quadroA != null ? quadroA.getTotalDebitos() : 0;
        double icmsProprioAnexo09 = anexo09 != null ? anexo09.getTotalIcmsProprio() : 0;
        double totalCreditos = quadroA != null ? quadroA.getTotalCreditos() : 0;
        double icmsProprioAnexo08 = anexo08 != null ? anexo08.getTotalIcmsProprio() : 0;

        icmsProprio = totalDebitos - icmsProprioAnexo09 - totalCreditos -
                icmsProprioAnexo08 - saldoCredorAnterior + creditosNaoCompensaveisMesSeguinte;

        if (icmsProprio < 0) {
            saldoCredorMesSeguinte = Math.abs(icmsProprio);
            icmsProprio = 0;
        } else {
            saldoCredorMesSeguinte = 0;
        }
        totalIcmsProprio = icmsProprio;
    }
}
