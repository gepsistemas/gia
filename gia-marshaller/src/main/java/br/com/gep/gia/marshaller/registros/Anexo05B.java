package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo05AB;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Outras Saídas – Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X05B")
})
public class Anexo05B extends AnexoComDetalhe<Anexo05B, DetalheAnexo05AB> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo05AB> detalhes;

    private double total;

    public Anexo05B() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo05AB> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotal() {
        return total;
    }

    @Override
    public Anexo05B adicionarDetalhe(DetalheAnexo05AB detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        total += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo05B removerDetalhe(DetalheAnexo05AB detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        total -= detalhe.getValor();
        return this;
    }
}
