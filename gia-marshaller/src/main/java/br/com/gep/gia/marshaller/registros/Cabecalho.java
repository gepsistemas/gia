package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class Cabecalho {

    @Field(at = 0, length = 4, literal = "****")
    private String tp;

    @Field(at = 4, length = 2, literal = "08")
    private String versao;

    @Field(at = 6, length = 2, align = Align.RIGHT, padding = '0')
    private int diaInicio;

    @Field(at = 8, length = 2, align = Align.RIGHT, padding = '0')
    private int diaTermino;

    @Field(at = 10, length = 2, align = Align.RIGHT, padding = '0')
    private int mes;

    @Field(at = 12, length = 4)
    private int ano;

    @Field(at = 16, length = 10, align = Align.RIGHT, padding = '0')
    private String ie;

    @Field(at = 26, length = 4, align = Align.RIGHT, padding = '0')
    private int sequencia = 1;

    public Cabecalho() {
    }

    public Cabecalho(int diaInicio, int diaTermino, int mes, int ano, String ie, int sequencia) {
        this.diaInicio = diaInicio;
        this.diaTermino = diaTermino;
        this.mes = mes;
        this.ano = ano;
        this.ie = ie;
        this.sequencia = sequencia;
    }

    public Cabecalho(int diaInicio, int diaTermino, int mes, int ano, String ie) {
        this.diaInicio = diaInicio;
        this.diaTermino = diaTermino;
        this.mes = mes;
        this.ano = ano;
        this.ie = ie;
    }

    public int getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(int diaInicio) {
        this.diaInicio = diaInicio;
    }

    public int getDiaTermino() {
        return diaTermino;
    }

    public void setDiaTermino(int diaTermino) {
        this.diaTermino = diaTermino;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public int getSequencia() {
        return sequencia;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }

    /**
     * Cria um novo Cabecalho com as mesmas propriedades deste, mas com a sequencia incrementada em 1
     */
    public Cabecalho proximo() {
        return new Cabecalho(diaInicio, diaTermino, mes, ano, ie, sequencia + 1);
    }
}
