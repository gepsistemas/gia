package br.com.gep.gia.marshaller.tipos;

public enum Natureza {
    TRANSPORTE("01"),
    ENERGIA_ELETRICA_DISTRIBUICAO("02"),
    COMUNICACAO("03"),
    AGUA("04"),
    VENDAS_FORA_DO_ESTABELECIMENTO("05"),
    ENERGIA_ELETRICA_GERACAO("06"),
    REGIME_ESPECIAL("09");

    private final String codigo;

    private Natureza(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
    
    public static Natureza parse(String codigo) {
        for (Natureza n : Natureza.values()) {
            if (n.getCodigo().equals(codigo))
                return n;
        }
        
        throw new UnsupportedOperationException("Não existe Natureza para o código '" + codigo + "'");
    }
}
