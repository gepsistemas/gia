package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public abstract class Anexo<T extends Registro> extends Registro<T> {

    @Field(at = 34, length = 2, align = Align.RIGHT, padding = '0')
    protected int ocorrencias;

    public int getOcorrencias() {
        return ocorrencias;
    }

    protected void setOcorrencias(int ocorrencias) {
        this.ocorrencias = ocorrencias;
    }
}
