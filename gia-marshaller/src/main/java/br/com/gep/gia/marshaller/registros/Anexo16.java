package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo16;
import br.com.gep.gia.marshaller.tipos.Natureza;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;
import org.beanio.builder.Align;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Operações Intermunicipais
 */
@Record(minOccurs = 0, maxOccurs = 7)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X16 ")
})
public class Anexo16 extends AnexoComDetalhe<Anexo16, DetalheAnexo16> {

    @Field(at = 34, length = 2, align = Align.RIGHT, padding = '0')
    private Natureza natureza;

    @Field(at = 36, length = 8, align = Align.RIGHT, padding = '0')
    private int atoDeclaratorio;

    @Field(at = 44, length = 16, handlerName = "double16")
    private double custos;

    @Field(at = 60, length = 2, align = Align.RIGHT, padding = '0')
    private int ocorrencias;

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo16> detalhes;

    public Anexo16() {
        detalhes = new ArrayList<>();
    }

    public Natureza getNatureza() {
        return natureza;
    }

    public Anexo16 setNatureza(Natureza natureza) {
        this.natureza = natureza;
        return this;
    }

    public int getAtoDeclaratorio() {
        return atoDeclaratorio;
    }

    public Anexo16 setAtoDeclaratorio(int atoDeclaratorio) {
        this.atoDeclaratorio = atoDeclaratorio;
        return this;
    }

    public double getCustos() {
        return custos;
    }

    Anexo16 setCustos(double custos) {
        this.custos = custos;
        return this;
    }

    @Override
    public int getOcorrencias() {
        return this.ocorrencias;
    }

    @Override
    protected void setOcorrencias(int ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public List<DetalheAnexo16> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    @Override
    public Anexo16 adicionarDetalhe(DetalheAnexo16 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        custos += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo16 removerDetalhe(DetalheAnexo16 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        custos -= detalhe.getValor();
        return this;
    }
}
