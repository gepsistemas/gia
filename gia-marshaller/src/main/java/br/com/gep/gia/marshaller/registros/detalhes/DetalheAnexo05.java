package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo05 {

    @Field(at = 36, length = 4, align = Align.RIGHT, padding = '0')
    private String cfop;

    @Field(at = 40, length = 13, handlerName = "double13")
    private double valorContabil;

    @Field(at = 53, length = 13, handlerName = "double13")
    private double baseCalculo;

    @Field(at = 66, length = 13, handlerName = "double13")
    private double debito;

    @Field(at = 79, length = 13, handlerName = "double13")
    private double isentas;

    @Field(at = 92, length = 13, handlerName = "double13")
    private double outras;

    @Field(at = 105, length = 16, handlerName = "double16")
    private double excluidas;

    public DetalheAnexo05() {
    }

    public DetalheAnexo05(String cfop, double valorContabil, double baseCalculo, double debito, double isentas, double outras, double excluidas) {
        this.cfop = cfop;
        this.valorContabil = valorContabil;
        this.baseCalculo = baseCalculo;
        this.debito = debito;
        this.isentas = isentas;
        this.outras = outras;
        this.excluidas = excluidas;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public double getValorContabil() {
        return valorContabil;
    }

    public void setValorContabil(double valorContabil) {
        this.valorContabil = valorContabil;
    }

    public double getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(double baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    public double getDebito() {
        return debito;
    }

    public void setDebito(double debito) {
        this.debito = debito;
    }

    public double getIsentas() {
        return isentas;
    }

    public void setIsentas(double isentas) {
        this.isentas = isentas;
    }

    public double getOutras() {
        return outras;
    }

    public void setOutras(double outras) {
        this.outras = outras;
    }

    public double getExcluidas() {
        return excluidas;
    }

    public void setExcluidas(double excluidas) {
        this.excluidas = excluidas;
    }
}
