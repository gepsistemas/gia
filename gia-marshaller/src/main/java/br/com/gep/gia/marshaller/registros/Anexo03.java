package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo03;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Créditos Presumidos – Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X03 ")
})
public class Anexo03 extends AnexoComDetalhe<Anexo03, DetalheAnexo03> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo03> detalhes;

    private double totalCreditos;

    public Anexo03() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo03> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    @Override
    public Anexo03 adicionarDetalhe(DetalheAnexo03 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalCreditos += detalhe.getValorCredito();
        return this;
    }

    @Override
    public Anexo03 removerDetalhe(DetalheAnexo03 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalCreditos -= detalhe.getValorCredito();
        return this;
    }
}
