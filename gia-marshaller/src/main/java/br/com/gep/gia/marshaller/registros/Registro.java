package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Segment;

public abstract class Registro<T extends Registro> {

    @Segment
    protected Cabecalho cabecalho;

    public Cabecalho getCabecalho() {
        return cabecalho;
    }

    @SuppressWarnings("unchecked")
    public T setCabecalho(Cabecalho cabecalho) {
        this.cabecalho = cabecalho;
        return (T) this;
    }
}
