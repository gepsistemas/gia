package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo02e06;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação dos Créditos Recebidos por Transferências
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X02 ")
})
public class Anexo02 extends AnexoComDetalhe<Anexo02, DetalheAnexo02e06> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo02e06> detalhes;

    private double totalCreditos;

    public Anexo02() {
        detalhes = new ArrayList<DetalheAnexo02e06>();
    }

    public List<DetalheAnexo02e06> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    @Override
    public Anexo02 adicionarDetalhe(DetalheAnexo02e06 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalCreditos += detalhe.getValorCredito();
        return this;
    }

    @Override
    public Anexo02 removerDetalhe(DetalheAnexo02e06 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalCreditos -= detalhe.getValorCredito();
        return this;
    }
}
