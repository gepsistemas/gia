package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo02e06;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação dos Créditos Transferidos
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X06 ")
})
public class Anexo06 extends AnexoComDetalhe<Anexo06, DetalheAnexo02e06> {

    @Segment(minOccurs = 1, maxOccurs = Integer.MAX_VALUE, collection = List.class)
    private List<DetalheAnexo02e06> detalhes;

    private double totalDebitos;

    public Anexo06() {
        detalhes = new ArrayList<DetalheAnexo02e06>();
    }

    public List<DetalheAnexo02e06> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }

    @Override
    public Anexo06 adicionarDetalhe(DetalheAnexo02e06 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalDebitos += detalhe.getValorCredito();
        return this;
    }

    @Override
    public Anexo06 removerDetalhe(DetalheAnexo02e06 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalDebitos -= detalhe.getValorCredito();
        return this;
    }
}
