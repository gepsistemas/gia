package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo09 {

    @Field(at = 36, length = 2, align = Align.RIGHT, padding = '0')
    private int diaVencimento;

    @Field(at = 38, length = 13, handlerName = "double13")
    private double icmsProprio;

    @Field(at = 51, length = 13, handlerName = "double13")
    private double icmsSt;

    @Field(at = 64, length = 10, align = Align.RIGHT, padding = '0')
    private String ie;

    public DetalheAnexo09() {
    }

    public DetalheAnexo09(int diaVencimento, double icmsProprio, double icmsSt, String ie) {
        this.diaVencimento = diaVencimento;
        this.icmsProprio = icmsProprio;
        this.icmsSt = icmsSt;
        this.ie = ie;
    }

    public int getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(int diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public double getIcmsProprio() {
        return icmsProprio;
    }

    public void setIcmsProprio(double icmsProprio) {
        this.icmsProprio = icmsProprio;
    }

    public double getIcmsSt() {
        return icmsSt;
    }

    public void setIcmsSt(double icmsSt) {
        this.icmsSt = icmsSt;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public double getTotalIcms() {
        return icmsProprio + icmsSt;
    }
}
