package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.*;
import org.beanio.annotation.Group;
import org.beanio.annotation.Record;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Group(maxOccurs = 1)
public class Estrutura {

    @Record(order = 1, minOccurs = 1, maxOccurs = 1)
    private Principal principal;

    @Record(order = 2, minOccurs = 0, maxOccurs = 1)
    private Observacoes observacoes;

    @Record(order = 3, minOccurs = 1, maxOccurs = 1)
    private Contribuinte contribuinte;

    @Record(order = 4, minOccurs = 0, maxOccurs = 1)
    private Anexo01 anexo01;

    @Record(order = 5, minOccurs = 0, maxOccurs = 1)
    private Anexo01C anexo01C;

    @Record(order = 6, minOccurs = 0, maxOccurs = 1)
    private Anexo02 anexo02;

    @Record(order = 7, minOccurs = 0, maxOccurs = 1)
    private Anexo03 anexo03;

    @Record(order = 8, minOccurs = 0, maxOccurs = 1)
    private Anexo04 anexo04;

    @Record(order = 9, minOccurs = 0, maxOccurs = 1)
    private Anexo05 anexo05;

    @Record(order = 10, minOccurs = 0, maxOccurs = 1)
    private Anexo05A anexo05A;

    @Record(order = 11, minOccurs = 0, maxOccurs = 1)
    private Anexo05B anexo05B;

    @Record(order = 12, minOccurs = 0, maxOccurs = 1)
    private Anexo05C anexo05C;

    @Record(order = 13, minOccurs = 0, maxOccurs = 1)
    private Anexo06 anexo06;

    @Record(order = 14, minOccurs = 0, maxOccurs = 1)
    private Anexo07 anexo07;

    @Record(order = 15, minOccurs = 0, maxOccurs = 1)
    private Anexo07A anexo07A;

    @Record(order = 16, minOccurs = 0, maxOccurs = 1)
    private Anexo07B anexo07B;

    @Record(order = 17, minOccurs = 0, maxOccurs = 1)
    private Anexo08 anexo08;

    @Record(order = 18, minOccurs = 0, maxOccurs = 1)
    private Anexo09 anexo09;

    @Record(order = 19, minOccurs = 0, maxOccurs = 1)
    private Anexo10 anexo10;

    @Record(order = 20, minOccurs = 0, maxOccurs = 1)
    private Anexo14 anexo14;

    @Record(order = 21, minOccurs = 0, maxOccurs = 1)
    private Anexo15 anexo15;

    @Record(order = 22, minOccurs = 0, maxOccurs = 7, collection = List.class)
    private List<Anexo16> anexos16;

    private Cabecalho cabecalho;

    public Estrutura() {
        cabecalho = new Cabecalho();
        principal = new Principal();
        contribuinte = new Contribuinte();
        anexos16 = new LinkedList<Anexo16>();
    }

    public Estrutura(Cabecalho cabecalho) {
        this();
        this.cabecalho = cabecalho;
    }

    public Cabecalho getCabecalho() {
        return cabecalho;
    }

    public void setCabecalho(Cabecalho cabecalho) {
        this.cabecalho = cabecalho;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public Contribuinte getContribuinte() {
        return contribuinte;
    }

    public void setContribuinte(Contribuinte contribuinte) {
        this.contribuinte = contribuinte;
    }

    public Anexo01 getAnexo01() {
        return anexo01;
    }

    public void setAnexo01(Anexo01 anexo01) {
        this.anexo01 = anexo01;
    }

    public Anexo01C getAnexo01C() {
        return anexo01C;
    }

    public void setAnexo01C(Anexo01C anexo01C) {
        this.anexo01C = anexo01C;
    }

    public Anexo02 getAnexo02() {
        return anexo02;
    }

    public void setAnexo02(Anexo02 anexo02) {
        this.anexo02 = anexo02;
    }

    public Anexo03 getAnexo03() {
        return anexo03;
    }

    public void setAnexo03(Anexo03 anexo03) {
        this.anexo03 = anexo03;
    }

    public Anexo04 getAnexo04() {
        return anexo04;
    }

    public void setAnexo04(Anexo04 anexo04) {
        this.anexo04 = anexo04;
    }

    public Anexo05 getAnexo05() {
        return anexo05;
    }

    public void setAnexo05(Anexo05 anexo05) {
        this.anexo05 = anexo05;
    }

    public Anexo05A getAnexo05A() {
        return anexo05A;
    }

    public void setAnexo05A(Anexo05A anexo05A) {
        this.anexo05A = anexo05A;
    }

    public Anexo05B getAnexo05B() {
        return anexo05B;
    }

    public void setAnexo05B(Anexo05B anexo05B) {
        this.anexo05B = anexo05B;
    }

    public Anexo05C getAnexo05C() {
        return anexo05C;
    }

    public void setAnexo05C(Anexo05C anexo05C) {
        this.anexo05C = anexo05C;
    }

    public Anexo06 getAnexo06() {
        return anexo06;
    }

    public void setAnexo06(Anexo06 anexo06) {
        this.anexo06 = anexo06;
    }

    public Anexo07 getAnexo07() {
        return anexo07;
    }

    public void setAnexo07(Anexo07 anexo07) {
        this.anexo07 = anexo07;
    }

    public Anexo07A getAnexo07A() {
        return anexo07A;
    }

    public void setAnexo07A(Anexo07A anexo07A) {
        this.anexo07A = anexo07A;
    }

    public Anexo07B getAnexo07B() {
        return anexo07B;
    }

    public void setAnexo07B(Anexo07B anexo07B) {
        this.anexo07B = anexo07B;
    }

    public Anexo08 getAnexo08() {
        return anexo08;
    }

    public void setAnexo08(Anexo08 anexo08) {
        this.anexo08 = anexo08;
    }

    public Anexo09 getAnexo09() {
        return anexo09;
    }

    public void setAnexo09(Anexo09 anexo09) {
        this.anexo09 = anexo09;
    }

    public Anexo10 getAnexo10() {
        return anexo10;
    }

    public void setAnexo10(Anexo10 anexo10) {
        this.anexo10 = anexo10;
    }

    public Anexo14 getAnexo14() {
        return anexo14;
    }

    public void setAnexo14(Anexo14 anexo14) {
        this.anexo14 = anexo14;
    }

    public Anexo15 getAnexo15() {
        return anexo15;
    }

    public void setAnexo15(Anexo15 anexo15) {
        this.anexo15 = anexo15;
    }

    public List<Anexo16> getAnexos16() {
        return anexos16;
    }

    public void setAnexos16(List<Anexo16> anexos16) {
        this.anexos16 = anexos16;
    }

    public Observacoes getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(Observacoes observacoes) {
        this.observacoes = observacoes;
    }

    /**
     * Calcula e atualiza registros associados.
     * Este método deve ser invocado quando a estrutura já estiver montada.
     */
    public void atualizarRegistros() {
        principal.getQuadroA().aplicarRegras(
                anexo01, anexo02, anexo03, anexo04, anexo05,
                anexo06, anexo07, anexo14, anexo15);

        principal.getQuadroB().aplicarRegras(
                anexo07, anexo08, anexo09, principal.getQuadroA());

        if (anexo10 == null) criarAnexo10();

        principal.getQuadroC().aplicarRegras(anexo01, anexo05, anexo08);

        contarLinhas();
    }

    private void criarAnexo10() {
        anexo10 = new Anexo10();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date vencimento = null;
        try {
            vencimento = sdf.parse("12/" + (cabecalho.getMes() + 1) + "/" + cabecalho.getAno());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DetalheAnexo08e10 detalheAnexo10 = new DetalheAnexo08e10(
                cabecalho.getDiaInicio(), cabecalho.getDiaTermino(),
                vencimento,
                principal.getQuadroB().getTotalIcmsProprio(), 0, "");

        anexo10.adicionarDetalhe(detalheAnexo10);
    }

    public void calcularFaturamento() {
        QuadroC quadroC = principal.getQuadroC();
        if (quadroC == null) return;

        double faturamento = 0;
        if (anexo05 != null) {
            for (DetalheAnexo05 detalhe : anexo05.getDetalhes()) {
                if (!cfopsParaSomarAoFaturamento.contains(detalhe.getCfop())) continue;

                faturamento += detalhe.getValorContabil();
            }
        }

        if (anexo01 != null) {
            for (DetalheAnexo01 detalhe : anexo01.getDetalhes()) {
                if (!cfopsParaSubtrairDoFaturamento.contains(detalhe.getCfop())) continue;

                faturamento -= detalhe.getValorContabil();
            }
        }

        quadroC.setFaturamentoMes(faturamento);
    }

    private void contarLinhas() {
        Totais totais = principal.getTotais();

        if (anexo01 != null) totais.setQtdeLinhasAnexo01(anexo01.getOcorrencias());

        if (anexo01C != null) totais.setQtdeLinhasAnexo01C(anexo01C.getOcorrencias());

        if (anexo02 != null) totais.setQtdeLinhasAnexo02(anexo02.getOcorrencias());

        if (anexo03 != null) totais.setQtdeLinhasAnexo03(anexo03.getOcorrencias());

        if (anexo04 != null) totais.setQtdeLinhasAnexo04(anexo04.getOcorrencias());

        if (anexo05 != null) totais.setQtdeLinhasAnexo05(anexo05.getOcorrencias());

        if (anexo05A != null) totais.setQtdeLinhasAnexo05A(anexo05A.getOcorrencias());

        if (anexo05B != null) totais.setQtdeLinhasAnexo05B(anexo05B.getOcorrencias());

        if (anexo05C != null) totais.setQtdeLinhasAnexo05C(anexo05C.getOcorrencias());

        if (anexo06 != null) totais.setQtdeLinhasAnexo06(anexo06.getOcorrencias());

        if (anexo07 != null) totais.setQtdeLinhasAnexo07(anexo07.getOcorrencias());

        if (anexo07A != null) totais.setQtdeLinhasAnexo07A(anexo07A.getOcorrencias());

        if (anexo07B != null) totais.setQtdeLinhasAnexo07B(anexo07B.getOcorrencias());

        if (anexo08 != null) totais.setQtdeLinhasAnexo08(anexo08.getOcorrencias());

        if (anexo09 != null) totais.setQtdeLinhasAnexo09(anexo09.getOcorrencias());

        if (anexo10 != null) totais.setQtdeLinhasAnexo10(anexo10.getOcorrencias());

        if (anexo14 != null) totais.setQtdeLinhasAnexo14(anexo14.getOcorrencias());

        if (anexo15 != null) totais.setQtdeLinhasAnexo15(anexo15.getOcorrencias());

        if (anexos16 != null && !anexos16.isEmpty()) {
            int qtdeLinhasAnexo16 = 0;
            for (Anexo16 anexo16 : anexos16) {
                qtdeLinhasAnexo16 += anexo16.getOcorrencias();
            }
            totais.setQtdeLinhasAnexo16(qtdeLinhasAnexo16);
        }
    }

    /**
     * Replica o cabeçalho da estrutura em todos os registros, setando a sequência correta.
     * Este método deve ser invocado quando a estrutura já estiver montada.
     */
    public void ajustarCabecalhos() {
        principal.setDiaInicio(cabecalho.getDiaInicio());
        principal.setDiaTermino(cabecalho.getDiaTermino());
        principal.setMes(cabecalho.getMes());
        principal.setAno(cabecalho.getAno());
        principal.setIe(cabecalho.getIe());

        List<Registro> registros = new LinkedList<Registro>(Arrays.asList(
                observacoes, contribuinte, anexo01, anexo01C, anexo02, anexo03, anexo04,
                anexo05, anexo05A, anexo05B, anexo05C, anexo06, anexo07, anexo07A, anexo07B,
                anexo08, anexo09, anexo10, anexo14, anexo15
        ));

        if (anexos16 != null && !anexos16.isEmpty()) {
            registros.addAll(anexos16);
        }

        cabecalho.setSequencia(1);

        for (Registro registro : registros) {
            if (registro != null) {
                cabecalho = cabecalho.proximo();
                registro.setCabecalho(cabecalho);
            }
        }
    }

    private List<String> cfopsParaSomarAoFaturamento = Arrays.asList(
            "5101", "5102", "5105", "5106", "5109", "5110", "5111", "5112", "5113", "5114", "5115", "5116",
            "5117", "5118", "5119", "5120", "5122", "5123", "5124", "5125", "5251", "5252", "5253", "5254",
            "5255", "5256", "5257", "5258", "5301", "5302", "5303", "5304", "5305", "5306", "5307", "5351",
            "5352", "5353", "5354", "5355", "5356", "5357", "5359", "5360", "5401", "5402", "5403", "5405",
            "5414", "5415", "5501", "5502", "5651", "5652", "5653", "5654", "5655", "5656", "5657", "5667",
            "5904", "5914", "5922", "5932", "5933", "6101", "6102", "6105", "6106", "6107", "6108", "6109",
            "6110", "6111", "6112", "6113", "6114", "6115", "6116", "6117", "6118", "6119", "6120", "6122",
            "6123", "6124", "6125", "6251", "6252", "6253", "6254", "6255", "6256", "6257", "6258", "6301",
            "6302", "6303", "6304", "6305", "6306", "6307", "6351", "6352", "6353", "6354", "6355", "6356",
            "6357", "6359", "6360", "6401", "6402", "6403", "6404", "6414", "6415", "6501", "6502", "6651",
            "6652", "6653", "6654", "6655", "6656", "6657", "6667", "6904", "6914", "6922", "6932", "6933",
            "7101", "7102", "7105", "7106", "7127", "7251", "7301", "7358", "7501", "7651", "7654", "7667"
    );

    private List<String> cfopsParaSubtrairDoFaturamento = Arrays.asList(
            "1201", "1202", "1203", "1204", "1205", "1206", "1207", "1410", "1411", "1414", "1415", "1503",
            "1504", "1660", "1661", "1662", "1904", "1914", "2201", "2202", "2203", "2204", "2205", "2206",
            "2207", "2410", "2411", "2414", "2415", "2503", "2504", "2660", "2661", "2662", "2904", "2914",
            "3201", "3202", "3205", "3206", "3207", "3211", "3503"
    );
}
