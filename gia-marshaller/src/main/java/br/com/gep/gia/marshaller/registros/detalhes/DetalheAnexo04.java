package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

import java.util.Date;

public class DetalheAnexo04 {

    @Field(at = 36, length = 2, align = Align.RIGHT, padding = '0')
    private int diaInicioApuracao;

    @Field(at = 38, length = 2, align = Align.RIGHT, padding = '0')
    private int diaTerminoApuracao;

    @Field(at = 40, length = 2, align = Align.RIGHT, padding = '0')
    private int mesApuracao;

    @Field(at = 42, length = 4)
    private int anoApuracao;

    @Field(at = 46, length = 8)
    private Date vencimento;

    @Field(at = 54, length = 13, handlerName = "double13")
    private double valorDevido;

    @Field(at = 67, length = 13, handlerName = "double13")
    private double valorPago;

    public DetalheAnexo04() {
    }

    public DetalheAnexo04(int diaInicioApuracao, int diaTerminoApuracao, int mesApuracao, int anoApuracao, Date vencimento, double valorDevido, double valorPago) {
        this.diaInicioApuracao = diaInicioApuracao;
        this.diaTerminoApuracao = diaTerminoApuracao;
        this.mesApuracao = mesApuracao;
        this.anoApuracao = anoApuracao;
        this.vencimento = vencimento;
        this.valorDevido = valorDevido;
        this.valorPago = valorPago;
    }

    public int getDiaInicioApuracao() {
        return diaInicioApuracao;
    }

    public void setDiaInicioApuracao(int diaInicioApuracao) {
        this.diaInicioApuracao = diaInicioApuracao;
    }

    public int getDiaTerminoApuracao() {
        return diaTerminoApuracao;
    }

    public void setDiaTerminoApuracao(int diaTerminoApuracao) {
        this.diaTerminoApuracao = diaTerminoApuracao;
    }

    public int getMesApuracao() {
        return mesApuracao;
    }

    public void setMesApuracao(int mesApuracao) {
        this.mesApuracao = mesApuracao;
    }

    public int getAnoApuracao() {
        return anoApuracao;
    }

    public void setAnoApuracao(int anoApuracao) {
        this.anoApuracao = anoApuracao;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public double getValorDevido() {
        return valorDevido;
    }

    public void setValorDevido(double valorDevido) {
        this.valorDevido = valorDevido;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public double getDiferenca() {
        return valorPago - valorDevido;
    }
}
