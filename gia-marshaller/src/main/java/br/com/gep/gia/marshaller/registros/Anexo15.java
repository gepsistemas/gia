package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo14e15;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Outros Débitos - Detalhamento
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X15 ")
})
public class Anexo15 extends AnexoComDetalhe<Anexo15, DetalheAnexo14e15> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo14e15> detalhes;

    public Anexo15() {
        detalhes = new ArrayList<>();
    }

    private double totalDebitos;

    public List<DetalheAnexo14e15> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }

    @Override
    public Anexo15 adicionarDetalhe(DetalheAnexo14e15 detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalDebitos += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo15 removerDetalhe(DetalheAnexo14e15 detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalDebitos -= detalhe.getValor();
        return this;
    }
}
