package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;

public class QuadroE {

    @Field(at = 606, length = 16, handlerName = "double16")
    private double estoqueInicialTributado;

    @Field(at = 622, length = 16, handlerName = "double16")
    private double estoqueInicialIsento;

    @Field(at = 638, length = 16, handlerName = "double16")
    private double estoqueInicialComTerceiros;

    @Field(at = 654, length = 16, handlerName = "double16")
    private double estoqueInicialDeTerceiros;

    @Field(at = 670, length = 16, handlerName = "double16")
    private double estoqueFinalTributado;

    @Field(at = 686, length = 16, handlerName = "double16")
    private double estoqueFinalIsento;

    @Field(at = 702, length = 16, handlerName = "double16")
    private double estoqueFinalComTerceiros;

    @Field(at = 718, length = 16, handlerName = "double16")
    private double estoqueFinalDeTerceiros;

    public double getEstoqueInicialTributado() {
        return estoqueInicialTributado;
    }

    public void setEstoqueInicialTributado(double estoqueInicialTributado) {
        this.estoqueInicialTributado = estoqueInicialTributado;
    }

    public double getEstoqueInicialIsento() {
        return estoqueInicialIsento;
    }

    public void setEstoqueInicialIsento(double estoqueInicialIsento) {
        this.estoqueInicialIsento = estoqueInicialIsento;
    }

    public double getEstoqueInicialComTerceiros() {
        return estoqueInicialComTerceiros;
    }

    public void setEstoqueInicialComTerceiros(double estoqueInicialComTerceiros) {
        this.estoqueInicialComTerceiros = estoqueInicialComTerceiros;
    }

    public double getEstoqueInicialDeTerceiros() {
        return estoqueInicialDeTerceiros;
    }

    public void setEstoqueInicialDeTerceiros(double estoqueInicialDeTerceiros) {
        this.estoqueInicialDeTerceiros = estoqueInicialDeTerceiros;
    }

    public double getEstoqueFinalTributado() {
        return estoqueFinalTributado;
    }

    public void setEstoqueFinalTributado(double estoqueFinalTributado) {
        this.estoqueFinalTributado = estoqueFinalTributado;
    }

    public double getEstoqueFinalIsento() {
        return estoqueFinalIsento;
    }

    public void setEstoqueFinalIsento(double estoqueFinalIsento) {
        this.estoqueFinalIsento = estoqueFinalIsento;
    }

    public double getEstoqueFinalComTerceiros() {
        return estoqueFinalComTerceiros;
    }

    public void setEstoqueFinalComTerceiros(double estoqueFinalComTerceiros) {
        this.estoqueFinalComTerceiros = estoqueFinalComTerceiros;
    }

    public double getEstoqueFinalDeTerceiros() {
        return estoqueFinalDeTerceiros;
    }

    public void setEstoqueFinalDeTerceiros(double estoqueFinalDeTerceiros) {
        this.estoqueFinalDeTerceiros = estoqueFinalDeTerceiros;
    }
}
