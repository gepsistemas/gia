package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo02e06 {

    @Field(at = 36, length = 10, align = Align.RIGHT, padding = '0')
    private String ie;

    @Field(at = 46, length = 3, align = Align.RIGHT, padding = '0')
    private String codigo;

    @Field(at = 49, length = 13, handlerName = "double13")
    private double valorCredito;

    public DetalheAnexo02e06() {
    }

    public DetalheAnexo02e06(String ie, String codigo, double valorCredito) {
        this.ie = ie;
        this.codigo = codigo;
        this.valorCredito = valorCredito;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(double valorCredito) {
        this.valorCredito = valorCredito;
    }
}
