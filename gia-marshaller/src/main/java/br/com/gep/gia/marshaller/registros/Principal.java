package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.*;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;
import org.beanio.builder.Align;

@Record(minOccurs = 1, maxOccurs = 1)
@Fields({
        @Field(at = 0, name = "TP", literal = "****"),
        @Field(at = 4, name = "versao", literal = "08"),
        @Field(at = 6, name = "dataEntrega", literal = "00000000"),
        @Field(at = 35, name = "sequencia", literal = "1", length = 4, align = Align.RIGHT, padding = '0'),
        @Field(at = 39, name = "id", rid = true, literal = "ABCE")
})
public class Principal {

    @Field(at = 14, length = 1)
    private boolean retificacao;

    @Field(at = 15, length = 2, align = Align.RIGHT, padding = '0')
    private int diaInicio;

    @Field(at = 17, length = 2, align = Align.RIGHT, padding = '0')
    private int diaTermino;

    @Field(at = 19, length = 2, align = Align.RIGHT, padding = '0')
    private int mes;

    @Field(at = 21, length = 4)
    private int ano;

    @Field(at = 25, length = 10, align = Align.RIGHT, padding = '0')
    private String ie;

    @Segment(minOccurs = 1, maxOccurs = 1)
    private QuadroA quadroA;

    @Segment(minOccurs = 1, maxOccurs = 1)
    private QuadroB quadroB;

    @Segment(minOccurs = 1, maxOccurs = 1)
    private QuadroC quadroC;

    @Segment(minOccurs = 1, maxOccurs = 1)
    private QuadroE quadroE;

    @Segment(minOccurs = 1, maxOccurs = 1)
    private Totais totais;

    public Principal() {
        quadroA = new QuadroA();
        quadroB = new QuadroB();
        quadroC = new QuadroC();
        quadroE = new QuadroE();
        totais = new Totais();
    }

    public boolean isRetificacao() {
        return retificacao;
    }

    public void setRetificacao(boolean retificacao) {
        this.retificacao = retificacao;
    }

    public int getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(int diaInicio) {
        this.diaInicio = diaInicio;
    }

    public int getDiaTermino() {
        return diaTermino;
    }

    public void setDiaTermino(int diaTermino) {
        this.diaTermino = diaTermino;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public QuadroA getQuadroA() {
        return quadroA;
    }

    public void setQuadroA(QuadroA quadroA) {
        this.quadroA = quadroA;
    }

    public QuadroB getQuadroB() {
        return quadroB;
    }

    public void setQuadroB(QuadroB quadroB) {
        this.quadroB = quadroB;
    }

    public QuadroC getQuadroC() {
        return quadroC;
    }

    public void setQuadroC(QuadroC quadroC) {
        this.quadroC = quadroC;
    }

    public QuadroE getQuadroE() {
        return quadroE;
    }

    public void setQuadroE(QuadroE quadroE) {
        this.quadroE = quadroE;
    }

    public Totais getTotais() {
        return totais;
    }

    public void setTotais(Totais totais) {
        this.totais = totais;
    }
}
