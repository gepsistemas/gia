package br.com.gep.gia.marshaller.handlers;

import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

public class SNTypeHandler implements TypeHandler {
    @Override
    public Object parse(String text) throws TypeConversionException {
        return "S".equals(text);
    }

    @Override
    public String format(Object value) {
        return value != null && ((Boolean)value).booleanValue() ? "S" : "N";
    }

    @Override
    public Class<?> getType() {
        return Boolean.class;
    }
}
