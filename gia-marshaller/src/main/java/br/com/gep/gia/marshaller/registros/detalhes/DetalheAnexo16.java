package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo16 {

    @Field(at = 62, length = 3, align = Align.RIGHT, padding = '0')
    private int municipio;

    @Field(at = 65, length = 16, handlerName = "double16")
    private double valor;

    public DetalheAnexo16() {
    }

    public DetalheAnexo16(int municipio, double valor) {
        this.municipio = municipio;
        this.valor = valor;
    }

    public int getMunicipio() {
        return municipio;
    }

    public void setMunicipio(int municipio) {
        this.municipio = municipio;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
