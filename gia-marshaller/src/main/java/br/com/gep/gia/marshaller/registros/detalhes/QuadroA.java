package br.com.gep.gia.marshaller.registros.detalhes;

import br.com.gep.gia.marshaller.registros.*;
import org.beanio.annotation.Field;

public class QuadroA {

    @Field(at = 43, length = 15, handlerName = "double15")
    private double creditoEntradas;

    @Field(at = 58, length = 15, handlerName = "double15")
    private double creditoImportacao;

    @Field(at = 73, length = 15, handlerName = "double15")
    private double creditoTransferencias;

    @Field(at = 88, length = 15, handlerName = "double15")
    private double creditosPresumidos;

    @Field(at = 103, length = 15, handlerName = "double15")
    private double creditosCompensacao;

    @Field(at = 118, length = 15, handlerName = "double15")
    private double outrosCreditos;

    @Field(at = 133, length = 15, handlerName = "double15")
    private double totalCreditos;

    @Field(at = 148, length = 15, handlerName = "double15")
    private double debitosSaidas;

    @Field(at = 163, length = 15, handlerName = "double15")
    private double debitosImportacao;

    @Field(at = 178, length = 15, handlerName = "double15")
    private double debitosCompensaveis;

    @Field(at = 193, length = 15, handlerName = "double15")
    private double debitosTransferencias;

    @Field(at = 208, length = 15, handlerName = "double15")
    private double debitosCompensacao;

    @Field(at = 223, length = 15, handlerName = "double15")
    private double outrosDebitos;

    @Field(at = 238, length = 15, handlerName = "double15")
    private double totalDebitos;

    @Field(at = 253, length = 1)
    private boolean realizouOperacoesST;

    public double getCreditoEntradas() {
        return creditoEntradas;
    }

    public void setCreditoEntradas(double creditoEntradas) {
        totalCreditos -= this.creditoEntradas;
        totalCreditos += creditoEntradas;
        this.creditoEntradas = creditoEntradas;
    }

    public double getCreditoImportacao() {
        return creditoImportacao;
    }

    public void setCreditoImportacao(double creditoImportacao) {
        totalCreditos -= this.creditoImportacao;
        totalCreditos += creditoImportacao;
        this.creditoImportacao = creditoImportacao;
    }

    public double getCreditoTransferencias() {
        return creditoTransferencias;
    }

    public void setCreditoTransferencias(double creditoTransferencias) {
        totalCreditos -= this.creditoTransferencias;
        totalCreditos += creditoTransferencias;
        this.creditoTransferencias = creditoTransferencias;
    }

    public double getCreditosPresumidos() {
        return creditosPresumidos;
    }

    public void setCreditosPresumidos(double creditosPresumidos) {
        totalCreditos -= this.creditosPresumidos;
        totalCreditos += creditosPresumidos;
        this.creditosPresumidos = creditosPresumidos;
    }

    public double getCreditosCompensacao() {
        return creditosCompensacao;
    }

    public void setCreditosCompensacao(double creditosCompensacao) {
        totalCreditos -= this.creditosCompensacao;
        totalCreditos += creditosCompensacao;
        this.creditosCompensacao = creditosCompensacao;
    }

    public double getOutrosCreditos() {
        return outrosCreditos;
    }

    public void setOutrosCreditos(double outrosCreditos) {
        totalCreditos -= this.outrosCreditos;
        totalCreditos += outrosCreditos;
        this.outrosCreditos = outrosCreditos;
    }

    public double getTotalCreditos() {
        return totalCreditos;
    }

    void setTotalCreditos(double totalCreditos) {
        this.totalCreditos = totalCreditos;
    }

    public double getDebitosSaidas() {
        return debitosSaidas;
    }

    public void setDebitosSaidas(double debitosSaidas) {
        totalDebitos -= this.debitosSaidas;
        totalDebitos += debitosSaidas;
        this.debitosSaidas = debitosSaidas;
    }

    public double getDebitosImportacao() {
        return debitosImportacao;
    }

    public void setDebitosImportacao(double debitosImportacao) {
        totalDebitos -= this.debitosImportacao;
        totalDebitos += debitosImportacao;
        this.debitosImportacao = debitosImportacao;
    }

    public double getDebitosCompensaveis() {
        return debitosCompensaveis;
    }

    public void setDebitosCompensaveis(double debitosCompensaveis) {
        totalDebitos -= this.debitosCompensaveis;
        totalDebitos += debitosCompensaveis;
        this.debitosCompensaveis = debitosCompensaveis;
    }

    public double getDebitosTransferencias() {
        return debitosTransferencias;
    }

    public void setDebitosTransferencias(double debitosTransferencias) {
        totalDebitos -= this.debitosTransferencias;
        totalDebitos += debitosTransferencias;
        this.debitosTransferencias = debitosTransferencias;
    }

    public double getDebitosCompensacao() {
        return debitosCompensacao;
    }

    public void setDebitosCompensacao(double debitosCompensacao) {
        totalDebitos -= this.debitosCompensacao;
        totalDebitos += debitosCompensacao;
        this.debitosCompensacao = debitosCompensacao;
    }

    public double getOutrosDebitos() {
        return outrosDebitos;
    }

    public void setOutrosDebitos(double outrosDebitos) {
        totalDebitos -= this.outrosDebitos;
        totalDebitos += outrosDebitos;
        this.outrosDebitos = outrosDebitos;
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }

    void setTotalDebitos(double totalDebitos) {
        this.totalDebitos = totalDebitos;
    }

    public boolean isRealizouOperacoesST() {
        return realizouOperacoesST;
    }

    public void setRealizouOperacoesST(boolean realizouOperacoesST) {
        this.realizouOperacoesST = realizouOperacoesST;
    }

    public void aplicarRegras(Anexo... anexosRelacionados) {
        for (Anexo anexo : anexosRelacionados) {
            if (anexo == null) continue;

            if (anexo instanceof Anexo01)
                setCreditoEntradas(((Anexo01) anexo).getTotalCreditos());

            if (anexo instanceof Anexo02)
                setCreditoTransferencias(((Anexo02) anexo).getTotalCreditos());

            if (anexo instanceof Anexo03)
                setCreditosPresumidos(((Anexo03) anexo).getTotalCreditos());

            if (anexo instanceof Anexo04)
                setCreditosCompensacao(((Anexo04) anexo).getTotalCreditos());

            if (anexo instanceof Anexo05)
                setDebitosSaidas(((Anexo05) anexo).getTotalDebitos());

            if (anexo instanceof Anexo06)
                setDebitosTransferencias(((Anexo06) anexo).getTotalDebitos());

            if (anexo instanceof Anexo07)
                setRealizouOperacoesST(true);

            if (anexo instanceof Anexo14)
                setOutrosCreditos(((Anexo14) anexo).getTotalCreditos());

            if (anexo instanceof Anexo15)
                setOutrosDebitos(((Anexo15) anexo).getTotalDebitos());
        }
    }
}