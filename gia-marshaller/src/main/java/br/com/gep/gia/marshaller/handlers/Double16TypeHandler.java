package br.com.gep.gia.marshaller.handlers;

import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

import java.text.DecimalFormat;

public class Double16TypeHandler implements TypeHandler {

    @Override
    public Object parse(String text) throws TypeConversionException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String format(Object value) {
        if (value == null) return "0000000000000000";

        return new DecimalFormat("#00000000000000.00").format(value).replaceAll("[\\.,]", "");
    }

    @Override
    public Class<?> getType() {
        return Double.class;
    }
}
