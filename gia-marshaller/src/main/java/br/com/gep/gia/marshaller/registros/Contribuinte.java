package br.com.gep.gia.marshaller.registros;

import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.builder.Align;

@Record(minOccurs = 1, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "XDC ")
})
public class Contribuinte extends Registro {

    @Field(at = 34, length = 14)
    private String cnpj;

    @Field(at = 48, length = 50)
    private String razaoSocial;

    @Field(at = 98, length = 11, align = Align.RIGHT, padding = '0')
    private String telefone = "";

    @Field(at = 109, length = 1)
    private boolean entregaCompleta;

    @Field(at = 110, length = 6)
    private String codigoEntrega;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        if (telefone == null) telefone = "";
        this.telefone = telefone;
    }

    public boolean isEntregaCompleta() {
        return entregaCompleta;
    }

    public void setEntregaCompleta(boolean entregaCompleta) {
        this.entregaCompleta = entregaCompleta;
    }

    public String getCodigoEntrega() {
        return codigoEntrega;
    }

    public void setCodigoEntrega(String codigoEntrega) {
        this.codigoEntrega = codigoEntrega;
    }
}
