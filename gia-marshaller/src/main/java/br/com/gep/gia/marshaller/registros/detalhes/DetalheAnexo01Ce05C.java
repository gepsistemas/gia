package br.com.gep.gia.marshaller.registros.detalhes;

import org.beanio.annotation.Field;
import org.beanio.builder.Align;

public class DetalheAnexo01Ce05C {

    @Field(at = 36, length = 4, align = Align.RIGHT, padding = '0')
    private String cfop;

    @Field(at = 40, length = 3, align = Align.RIGHT, padding = '0')
    private int codigo;

    @Field(at = 43, length = 13, handlerName = "double13")
    private double valor;

    @Field(at = 56, length = 60)
    private String especificacao;

    public DetalheAnexo01Ce05C() {
    }

    public DetalheAnexo01Ce05C(String cfop, int codigo, double valor) {
        this.cfop = cfop;
        this.codigo = codigo;
        this.valor = valor;
    }

    public DetalheAnexo01Ce05C(String cfop, int codigo, double valor, String especificacao) {
        this(cfop, codigo, valor);
        this.especificacao = especificacao;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getEspecificacao() {
        return especificacao;
    }

    public void setEspecificacao(String especificacao) {
        this.especificacao = especificacao;
    }
}
