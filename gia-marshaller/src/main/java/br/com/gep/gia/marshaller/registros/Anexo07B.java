package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo07AB;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Discriminação das Operações de Saída com Substituição Tributária
 */
@Record(minOccurs = 0, maxOccurs = 1)
@Fields({
        @Field(at = 30, name = "id", rid = true, literal = "X07B")
})
public class Anexo07B extends AnexoComDetalhe<Anexo07B, DetalheAnexo07AB> {

    @Segment(minOccurs = 1, maxOccurs = 99, collection = List.class)
    private List<DetalheAnexo07AB> detalhes;

    private double totalDebitos;

    public Anexo07B() {
        detalhes = new ArrayList<>();
    }

    public List<DetalheAnexo07AB> getDetalhes() {
        return Collections.unmodifiableList(detalhes);
    }

    public double getTotalDebitos() {
        return totalDebitos;
    }

    @Override
    public Anexo07B adicionarDetalhe(DetalheAnexo07AB detalhe) {
        detalhes.add(detalhe);
        ocorrencias++;
        totalDebitos += detalhe.getValor();
        return this;
    }

    @Override
    public Anexo07B removerDetalhe(DetalheAnexo07AB detalhe) {
        detalhes.remove(detalhe);
        ocorrencias--;
        totalDebitos -= detalhe.getValor();
        return this;
    }
}
