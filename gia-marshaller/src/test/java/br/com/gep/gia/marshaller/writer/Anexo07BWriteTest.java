package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo07B;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo07AB;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo07BWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo07B anexo = new Anexo07B();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X07B"));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo07B anexo = new Anexo07B();
        anexo.adicionarDetalhe(new DetalheAnexo07AB("5555", 0D, 0D));
        anexo.adicionarDetalhe(new DetalheAnexo07AB("6666", 0D, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 96);
        assertThat(conteudo, is("02555500000000000000000000000000666600000000000000000000000000"));
    }
}
