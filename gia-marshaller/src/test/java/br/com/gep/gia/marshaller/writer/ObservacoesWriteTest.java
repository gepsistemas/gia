package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.Observacoes;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ObservacoesWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Observacoes obs = new Observacoes();
        obs.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(obs);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002OBS "));
    }

    @Test
    public void escreveConteudo() throws IOException {
        Observacoes obs = new Observacoes();
        obs.setTexto("ObservacaoObservacaoObservacaoObservacaoObservacaoObservacao");

        writer.writeAndFlush(obs);

        String conteudo = stream.toString().substring(34, 136);
        assertThat(conteudo, is("02ObservacaoObservacaoObservacaoObservacaoObservacaoObservacao                                        "));
    }
}
