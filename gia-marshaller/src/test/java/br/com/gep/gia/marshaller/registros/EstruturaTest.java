package br.com.gep.gia.marshaller.registros;

import br.com.gep.gia.marshaller.ObjectBuilder;
import br.com.gep.gia.marshaller.registros.detalhes.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class EstruturaTest {

    @Test
    public void preencheQuadroA() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();

        QuadroA quadroA = estrutura.getPrincipal().getQuadroA();
        assertThat(quadroA.getCreditoEntradas()).isEqualTo(100);
        assertThat(quadroA.getCreditoTransferencias()).isEqualTo(10000);
        assertThat(quadroA.getCreditosPresumidos()).isEqualTo(1000);
        assertThat(quadroA.getCreditosCompensacao()).isEqualTo(1000);
        assertThat(quadroA.getOutrosCreditos()).isEqualTo(1000);
        assertThat(quadroA.getTotalCreditos()).isEqualTo(13100);
        assertThat(quadroA.getDebitosSaidas()).isEqualTo(180);
        assertThat(quadroA.getDebitosTransferencias()).isEqualTo(10000);
        assertThat(quadroA.getOutrosDebitos()).isEqualTo(1000);
        assertThat(quadroA.getTotalDebitos()).isEqualTo(11180);
        assertThat(quadroA.isRealizouOperacoesST()).isTrue();
    }

    @Test
    public void preencheQuadroB() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();

        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        assertThat(quadroB.getPagamentosMes()).isEqualTo(150);
        assertThat(quadroB.getDebitosNaoPagos()).isEqualTo(150);
    }

    @Test
    public void calculaIcmsStARecolherComValorNegativoNoQuadroB() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();

        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        assertThat(quadroB.getIcmsStRecolher()).isEqualTo(0);
        assertThat(quadroB.getSaldoCredorStMesSeguinte()).isEqualTo(100);
    }

    @Test
    public void calculaIcmsStARecolherComValorPositivoNoQuadroB() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.getAnexo07().setOutrosDebitos(1000D);
        estrutura.atualizarRegistros();

        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        assertThat(quadroB.getIcmsStRecolher()).isEqualTo(850);
        assertThat(quadroB.getSaldoCredorStMesSeguinte()).isEqualTo(0);
    }

    @Test
    public void calculaIcmsProprioComValorNegativoNoQuadroB() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        quadroB.setSaldoCredorAnterior(0);
        quadroB.setCreditosNaoCompensaveisMesSeguinte(0);
        estrutura.atualizarRegistros();

        assertThat(quadroB.getIcmsProprio()).isEqualTo(0);
        assertThat(quadroB.getSaldoCredorMesSeguinte()).isEqualTo(2120);
        assertThat(quadroB.getTotalIcmsProprio()).isEqualTo(0);
    }

    @Test
    public void calculaIcmsProprioComValorPositivoNoQuadroB() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        quadroB.setSaldoCredorAnterior(0);
        quadroB.setCreditosNaoCompensaveisMesSeguinte(3000);
        estrutura.atualizarRegistros();

        assertThat(quadroB.getIcmsProprio()).isEqualTo(880);
        assertThat(quadroB.getSaldoCredorMesSeguinte()).isEqualTo(0);
        assertThat(quadroB.getTotalIcmsProprio()).isEqualTo(880);
    }

    @Test
    public void calculaFaturamentoSeOMesmoNaoForInformado() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        QuadroC quadroC = estrutura.getPrincipal().getQuadroC();
        quadroC.setFaturamentoMes(0);
        estrutura.atualizarRegistros();
        estrutura.calcularFaturamento();

        assertThat(quadroC.getFaturamentoMes()).isEqualTo(3000);
    }

    @Test
    public void geraAnexo10() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        QuadroB quadroB = estrutura.getPrincipal().getQuadroB();
        quadroB.setSaldoCredorAnterior(0);
        quadroB.setCreditosNaoCompensaveisMesSeguinte(3000);
        estrutura.atualizarRegistros();

        Anexo10 anexo10 = estrutura.getAnexo10();
        assertThat(anexo10).isNotNull();
        assertThat(anexo10.getTotalGeral()).isEqualTo(880);
        assertThat(anexo10.getDetalhes()).hasSize(1);
    }

    @Test
    public void preencheQuadroC() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();

        QuadroC quadroC = estrutura.getPrincipal().getQuadroC();
        assertThat(quadroC.getTotalEntradas()).isEqualTo(4150000);
        assertThat(quadroC.getTotalSaidas()).isEqualTo(4000);
        assertThat(quadroC.getPagamentoFatoGeradorIcmsProprio()).isEqualTo(100);
        assertThat(quadroC.getPagamentoFatoGeradorIcmsST()).isEqualTo(50);
    }

    @Test
    public void calculaTotais() {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();

        Totais totais = estrutura.getPrincipal().getTotais();
        assertThat(totais.getQtdeLinhasAnexo01()).isEqualTo(3);
        assertThat(totais.getQtdeLinhasAnexo01C()).isEqualTo(3);
        assertThat(totais.getQtdeLinhasAnexo02()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo03()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo04()).isEqualTo(2);
        assertThat(totais.getQtdeLinhasAnexo05()).isEqualTo(3);
        assertThat(totais.getQtdeLinhasAnexo05A()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo05B()).isEqualTo(2);
        assertThat(totais.getQtdeLinhasAnexo05C()).isEqualTo(3);
        assertThat(totais.getQtdeLinhasAnexo06()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo07()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo07A()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo07B()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo08()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo09()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo10()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo14()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo15()).isEqualTo(1);
        assertThat(totais.getQtdeLinhasAnexo16()).isEqualTo(3);
    }
}
