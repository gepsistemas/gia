package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo14;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo14e15;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo14WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo14 anexo = new Anexo14();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X14 "));
    }

    @Test
    public void escreveCreditosParaDoisCodigos() throws IOException {
        Anexo14 anexo = new Anexo14();
        anexo.adicionarDetalhe(new DetalheAnexo14e15(1, 0D, ""));
        anexo.adicionarDetalhe(new DetalheAnexo14e15(99, 0D, "Especificacao"));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 188);
        assertThat(conteudo, is("020010000000000000                                                            0990000000000000Especificacao                                               "));
    }
}
