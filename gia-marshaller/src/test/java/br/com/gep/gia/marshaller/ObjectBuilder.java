package br.com.gep.gia.marshaller;

import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.registros.detalhes.*;
import br.com.gep.gia.marshaller.tipos.Natureza;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ObjectBuilder {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static Estrutura criarEstrutura() {
        Cabecalho cabecalho = new Cabecalho(1, 31, 7, 2014, "99999999");

        Date data = null;
        try {
            data = sdf.parse("10/07/2014");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Contribuinte contribuinte = new Contribuinte();
        contribuinte.setCnpj("99999999999999");
        contribuinte.setRazaoSocial("Empresa Teste");
        contribuinte.setTelefone("9999999999");
        contribuinte.setCodigoEntrega("999999");

        Anexo01C anexo01C = new Anexo01C()
                .adicionarDetalhe(new DetalheAnexo01Ce05C("1303", 5, 50000))
                .adicionarDetalhe(new DetalheAnexo01Ce05C("1403", 1, 50))
                .adicionarDetalhe(new DetalheAnexo01Ce05C("1403", 2, 50));

        Anexo01 anexo01 = new Anexo01()
                .adicionarDetalhe(new DetalheAnexo01("1253", 100000, 100000, 100, 0, 0, 0))
                .adicionarDetalhe(new DetalheAnexo01("1303", 50000, 0, 0, 50000, 0, 0))
                .adicionarDetalhe(new DetalheAnexo01("1403", 4000000, 0, 0, 0, 4000000, 0));

        Anexo02 anexo02 = new Anexo02()
                .adicionarDetalhe(new DetalheAnexo02e06("99999", "1", 10000));

        Anexo03 anexo03 = new Anexo03()
                .adicionarDetalhe(new DetalheAnexo03("1", 1000));

        Anexo04 anexo04 = new Anexo04()
                .adicionarDetalhe(new DetalheAnexo04(1, 31, 1, 2014, data, 300, 700))
                .adicionarDetalhe(new DetalheAnexo04(1, 28, 2, 2014, data, 200, 800));

        Anexo05 anexo05 = new Anexo05();

        Anexo05A anexo05A = new Anexo05A()
                .adicionarDetalhe(new DetalheAnexo05AB("5202", 1, 1000));

        Anexo05B anexo05B = new Anexo05B()
                .adicionarDetalhe(new DetalheAnexo05AB("5351", 1, 1000))
                .adicionarDetalhe(new DetalheAnexo05AB("5351", 2, 1000));

        Anexo05C anexo05C = new Anexo05C()
                .adicionarDetalhe(new DetalheAnexo01Ce05C("5101", 5, 1000))
                .adicionarDetalhe(new DetalheAnexo01Ce05C("5202", 1, 50))
                .adicionarDetalhe(new DetalheAnexo01Ce05C("5202", 2, 50));

        anexo05
                .adicionarDetalhe(new DetalheAnexo05("5101", 1000, 1000, 180, 0, 0, 0))
                .adicionarDetalhe(new DetalheAnexo05("5202", 1000, 0, 0, anexo05A.getTotal(), 0, 0))
                .adicionarDetalhe(new DetalheAnexo05("5351", 2000, 0, 0, 0, anexo05B.getTotal(), 0));

        Anexo06 anexo06 = new Anexo06()
                .adicionarDetalhe(new DetalheAnexo02e06("99999", "1", 10000));

        Anexo07 anexo07 = new Anexo07();

        Anexo07A anexo07A = new Anexo07A()
                .adicionarDetalhe(new DetalheAnexo07AB("1101", 100, 18));

        Anexo07B anexo07B = new Anexo07B()
                .adicionarDetalhe(new DetalheAnexo07AB("5101", 100, 18));

        anexo07.setCreditoEntradasSt(anexo07A.getTotalCreditos());
        anexo07.setOutrosCreditos(50D);
        anexo07.setTextoOutrosCreditos("Outros creditos");
        anexo07.setDebitoSaidasSt(anexo07B.getTotalDebitos());
        anexo07.setOutrosDebitos(50D);
        anexo07.setTextoOutrosDebitos("Outros debitos");

        Anexo08 anexo08 = new Anexo08()
                .adicionarDetalhe(new DetalheAnexo08e10(1, 31, data, 100, 50, "99999"));

        Anexo09 anexo09 = new Anexo09()
                .adicionarDetalhe(new DetalheAnexo09(10, 100, 50, "99999"));

//        Anexo10 anexo10 = new Anexo10()
//                .adicionarDetalhe(new DetalheAnexo08e10(1, 31, data, 100, 50, "99999"));

        Anexo14 anexo14 = new Anexo14()
                .adicionarDetalhe(new DetalheAnexo14e15(1, 1000, "Especificacao"));

        Anexo15 anexo15 = new Anexo15()
                .adicionarDetalhe(new DetalheAnexo14e15(1, 1000, "Especificacao"));

        Anexo16 anexo16Transporte = new Anexo16()
                .setNatureza(Natureza.TRANSPORTE)
                .adicionarDetalhe(new DetalheAnexo16(1, 1000))
                .adicionarDetalhe(new DetalheAnexo16(11, 500));

        Anexo16 anexo16Comunicacao = new Anexo16()
                .setNatureza(Natureza.COMUNICACAO)
                .adicionarDetalhe(new DetalheAnexo16(1, 1000));

        Observacoes observacoes = new Observacoes();
        observacoes.setTexto("Lorem ipsum sit met dolor");

        Principal principal = new Principal();
        principal.getQuadroC().setFaturamentoMes(1000000);
        principal.getQuadroC().setNumeroEmpregados(1000);
        principal.getQuadroE().setEstoqueInicialIsento(10000);
        principal.getQuadroE().setEstoqueFinalIsento(500);

        Estrutura estrutura = new Estrutura(cabecalho);
        estrutura.setPrincipal(principal);
        estrutura.setContribuinte(contribuinte);
        estrutura.setAnexo01(anexo01);
        estrutura.setAnexo01C(anexo01C);
        estrutura.setAnexo02(anexo02);
        estrutura.setAnexo03(anexo03);
        estrutura.setAnexo04(anexo04);
        estrutura.setAnexo05(anexo05);
        estrutura.setAnexo05A(anexo05A);
        estrutura.setAnexo05B(anexo05B);
        estrutura.setAnexo05C(anexo05C);
        estrutura.setAnexo06(anexo06);
        estrutura.setAnexo07(anexo07);
        estrutura.setAnexo07A(anexo07A);
        estrutura.setAnexo07B(anexo07B);
        estrutura.setAnexo08(anexo08);
        estrutura.setAnexo09(anexo09);
        //estrutura.setAnexo10(anexo10);
        estrutura.setAnexo14(anexo14);
        estrutura.setAnexo15(anexo15);
        estrutura.getAnexos16().addAll(Arrays.asList(anexo16Transporte, anexo16Comunicacao));
        estrutura.setObservacoes(observacoes);

        estrutura.ajustarCabecalhos();

        return estrutura;
    }
}
