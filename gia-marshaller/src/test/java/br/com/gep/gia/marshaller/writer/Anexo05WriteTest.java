package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo05;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo05;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo05WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo05 anexo = new Anexo05();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X05 "));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo05 anexo = new Anexo05();
        anexo.adicionarDetalhe(new DetalheAnexo05("5555", 0D, 0D, 0D, 0D, 0D, 0D));
        anexo.adicionarDetalhe(new DetalheAnexo05("6666", 0D, 0D, 0D, 0D, 0D, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 206);
        assertThat(conteudo, is("0255550000000000000000000000000000000000000000000000000000000000000000000000000000000006666000000000000000000000000000000000000000000000000000000000000000000000000000000000"));
    }
}
