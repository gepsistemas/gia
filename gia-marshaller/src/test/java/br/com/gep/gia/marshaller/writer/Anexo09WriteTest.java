package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo09;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo09;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo09WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo09 anexo = new Anexo09();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X09 "));
    }

    @Test
    public void escreveDebitosParaDoisVencimentos() throws IOException {
        Anexo09 anexo = new Anexo09();
        anexo.adicionarDetalhe(new DetalheAnexo09(1, 0D, 0D, "12345"));
        anexo.adicionarDetalhe(new DetalheAnexo09(10, 0D, 0D, "12345"));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 112);
        assertThat(conteudo, is("020100000000000000000000000000000001234510000000000000000000000000000000012345"));
    }
}
