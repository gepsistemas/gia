package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo01;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo01;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo01WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo01 anexo = new Anexo01();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X01 "));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo01 anexo = new Anexo01();
        anexo.adicionarDetalhe(new DetalheAnexo01("1111", 0D, 0D, 0D, 0D, 0D, 0D));
        anexo.adicionarDetalhe(new DetalheAnexo01("2222", 0D, 0D, 0D, 0D, 0D, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 206);
        assertThat(conteudo, is("0211110000000000000000000000000000000000000000000000000000000000000000000000000000000002222000000000000000000000000000000000000000000000000000000000000000000000000000000000"));
    }
}
