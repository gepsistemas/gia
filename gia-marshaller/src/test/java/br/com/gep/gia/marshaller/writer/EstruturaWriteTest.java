package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.ObjectBuilder;
import br.com.gep.gia.marshaller.registros.Estrutura;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static org.assertj.core.api.Assertions.*;

public class EstruturaWriteTest extends BaseWriteTest {

    @Test
    public void escreveArquivoCompleto() throws ParseException, IOException {
        Estrutura estrutura = ObjectBuilder.criarEstrutura();
        estrutura.atualizarRegistros();
        estrutura.ajustarCabecalhos();

        writer.writeAndFlush(estrutura);

        String[] linhas = stream.toString().split("\n");

        assertThat(linhas).hasSize(23);
    }
}
