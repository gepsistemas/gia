package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.Contribuinte;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ContribuinteWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Contribuinte contribuinte = new Contribuinte();
        contribuinte.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(contribuinte);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002XDC "));
    }

    @Test
    public void escreveConteudo() throws IOException {
        Contribuinte contribuinte = new Contribuinte();
        contribuinte.setCnpj("99999999999999");
        contribuinte.setRazaoSocial("Empresa Teste");
        contribuinte.setTelefone("09999999999");
        contribuinte.setCodigoEntrega("999");

        writer.writeAndFlush(contribuinte);

        String conteudo = stream.toString().substring(34, 116);
        assertThat(conteudo, is("99999999999999Empresa Teste                                     09999999999N999   "));
    }

    @Test
    public void preencheTelefoneComZerosSeForNulo() throws IOException {
        Contribuinte contribuinte = new Contribuinte();
        contribuinte.setCnpj("99999999999999");
        contribuinte.setRazaoSocial("Empresa Teste");
        contribuinte.setTelefone(null);
        contribuinte.setCodigoEntrega("999");

        writer.writeAndFlush(contribuinte);

        String conteudo = stream.toString().substring(34, 116);
        assertThat(conteudo, is("99999999999999Empresa Teste                                     00000000000N999   "));
    }
}
