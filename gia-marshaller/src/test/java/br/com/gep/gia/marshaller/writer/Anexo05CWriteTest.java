package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo05C;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo01Ce05C;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo05CWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo05C anexo = new Anexo05C();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X05C"));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo05C anexo = new Anexo05C();
        anexo.adicionarDetalhe(new DetalheAnexo01Ce05C("5555", 1, 0D, "Especificacao 1"));
        anexo.adicionarDetalhe(new DetalheAnexo01Ce05C("6666", 2, 0D, "Especificacao 2"));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 196);
        assertThat(conteudo,
                is("0255550010000000000000Especificacao 1                                             66660020000000000000Especificacao 2                                             "));
    }
}
