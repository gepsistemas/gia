package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo05A;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo05AB;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo05AWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo05A anexo = new Anexo05A();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X05A"));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo05A anexo = new Anexo05A();
        anexo.adicionarDetalhe(new DetalheAnexo05AB("5555", 1, 0D));
        anexo.adicionarDetalhe(new DetalheAnexo05AB("6666", 2, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 76);
        assertThat(conteudo, is("025555001000000000000066660020000000000000"));
    }
}
