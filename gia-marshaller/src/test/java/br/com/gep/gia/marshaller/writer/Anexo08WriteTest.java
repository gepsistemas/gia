package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo08;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo08e10;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo08WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo08 anexo = new Anexo08();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X08 "));
    }

    @Test
    public void escrevePagamentosParaDoisPeriodos() throws IOException, ParseException {
        Anexo08 anexo = new Anexo08();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        anexo.adicionarDetalhe(new DetalheAnexo08e10(1, 30, sdf.parse("20/02/2014"), 0D, 0D, "12345"));
        anexo.adicionarDetalhe(new DetalheAnexo08e10(1, 28, sdf.parse("20/03/2014"), 0D, 0D, "12345"));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 132);
        assertThat(conteudo, is("02013020022014000000000000000000000000000000012345012820032014000000000000000000000000000000012345"));
    }
}
