package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo01C;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo01Ce05C;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class Anexo01CWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo01C anexo = new Anexo01C();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho).isEqualTo("****08013001201400000123450002X01C");
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisCfops() throws IOException {
        Anexo01C anexo = new Anexo01C();
        anexo.adicionarDetalhe(new DetalheAnexo01Ce05C("1111", 1, 0D, "Especificacao 1"));
        anexo.adicionarDetalhe(new DetalheAnexo01Ce05C("2222", 2, 0D, "Especificacao 2"));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 196);
        assertThat(conteudo)
                .isEqualTo("0211110010000000000000Especificacao 1                                             22220020000000000000Especificacao 2                                             ");
    }
}
