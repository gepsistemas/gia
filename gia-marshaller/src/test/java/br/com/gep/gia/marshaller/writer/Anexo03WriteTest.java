package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo03;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo03;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo03WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo03 anexo = new Anexo03();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X03 "));
    }

    @Test
    public void escreveCreditosParaDoisCodigos() throws IOException {
        Anexo03 anexo = new Anexo03();
        anexo.adicionarDetalhe(new DetalheAnexo03("1", 50D));
        anexo.adicionarDetalhe(new DetalheAnexo03("9", 50D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 92);
        assertThat(conteudo, is("0200100000000050000000000000000090000000005000000000000000"));
    }
}
