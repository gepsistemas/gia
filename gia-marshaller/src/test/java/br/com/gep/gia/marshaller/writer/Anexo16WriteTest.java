package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo16;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo16;
import br.com.gep.gia.marshaller.tipos.Natureza;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo16WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo16 anexo = new Anexo16()
                .setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2))
                .setNatureza(Natureza.TRANSPORTE);

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 60);
        assertThat(cabecalho, is("****08013001201400000123450002X16 01000000000000000000000000"));
    }

    @Test
    public void escreveValoresParaDoisMunicipios() throws IOException {
        Anexo16 anexo = new Anexo16()
                .adicionarDetalhe(new DetalheAnexo16(1, 0D))
                .adicionarDetalhe(new DetalheAnexo16(2, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(60, 100);
        assertThat(conteudo, is("0200100000000000000000020000000000000000"));
    }
}
