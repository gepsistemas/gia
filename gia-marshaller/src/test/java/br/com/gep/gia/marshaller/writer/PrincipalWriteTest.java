package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Principal;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PrincipalWriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Principal reg = new Principal();
        reg.setRetificacao(false);
        reg.setDiaInicio(01);
        reg.setDiaTermino(30);
        reg.setMes(1);
        reg.setAno(2014);
        reg.setIe("12345");

        writer.writeAndFlush(reg);

        String cabecalho = stream.toString().substring(0, 43);
        assertThat(cabecalho, is("****0800000000N013001201400000123450001ABCE"));
    }

    @Test
    public void escreveQuadroA() throws IOException {
        Principal reg = new Principal();
        reg.getQuadroA().setCreditoEntradas(100.00);
        reg.getQuadroA().setRealizouOperacoesST(true);

        writer.writeAndFlush(reg);

        String quadroA = stream.toString().substring(43, 254);
        assertThat(quadroA, is("000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000S"));
    }

    @Test
    public void escreveQuadroB() throws IOException {
        Principal reg = new Principal();
        reg.getQuadroB().setSaldoCredorAnterior(100.00);
        reg.getQuadroB().setSaldoDevedorMesSeguinte(100.00);

        writer.writeAndFlush(reg);

        String quadroB = stream.toString().substring(254, 419);
        assertThat(quadroB, is("000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000"));
    }

    @Test
    public void escreveQuadroC() throws IOException {
        Principal reg = new Principal();
        reg.getQuadroC().setFaturamentoMes(100.00);
        reg.getQuadroC().setNumeroEmpregados(10);
        reg.getQuadroC().setTransportaSadoDevedor(true);

        writer.writeAndFlush(reg);

        String quadroC = stream.toString().substring(419, 606);
        assertThat(quadroC, is("000000000010000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000S"));
    }

    @Test
    public void escreveQuadroE() throws IOException {
        Principal reg = new Principal();
        reg.getQuadroE().setEstoqueInicialTributado(100.00);
        reg.getQuadroE().setEstoqueFinalDeTerceiros(100.00);

        writer.writeAndFlush(reg);

        String quadroE = stream.toString().substring(606, 734);
        assertThat(quadroE, is("00000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000"));
    }

    @Test
    public void escreveTotais() throws IOException {
        Principal reg = new Principal();
        reg.getTotais().setQtdeLinhasAnexo01(10);

        writer.writeAndFlush(reg);

        String totais = stream.toString().substring(734, 795);
        assertThat(totais, is("010000000000000000000000000000000000000000000000000000000SNNN"));
    }
}
