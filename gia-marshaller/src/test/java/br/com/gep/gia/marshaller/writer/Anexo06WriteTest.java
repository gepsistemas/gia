package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo06;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo02e06;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo06WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo06 anexo = new Anexo06();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X06 "));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisDestinos() throws IOException {
        Anexo06 anexo = new Anexo06();
        anexo.adicionarDetalhe(new DetalheAnexo02e06("12345", "10", 0D));
        anexo.adicionarDetalhe(new DetalheAnexo02e06("23456", "10", 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 88);
        assertThat(conteudo, is("020000012345010000000000000000000234560100000000000000"));
    }
}
