package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo07;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo07WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo07 anexo = new Anexo07();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X07 "));
    }

    @Test
    public void escreveDiscriminacaoDeValoresParaDoisDestinos() throws IOException {
        Anexo07 anexo = new Anexo07();
        anexo.setCreditoEntradasSt(100D);
        anexo.setTextoOutrosCreditos("Outros creditos");
        anexo.setDebitoSaidasSt(100D);
        anexo.setTextoOutrosDebitos("Outros debitos");


        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 234);
        assertThat(conteudo, is("0100000000100000000000000000Outros creditos                                             000000001000000000000100000000000000000Outros debitos                                              0000000010000"));
    }
}
