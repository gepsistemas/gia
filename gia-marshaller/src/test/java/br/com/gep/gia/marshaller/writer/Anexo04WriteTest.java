package br.com.gep.gia.marshaller.writer;

import br.com.gep.gia.marshaller.registros.Anexo04;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo04;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Anexo04WriteTest extends BaseWriteTest {

    @Test
    public void escreveCabecalho() throws IOException {
        Anexo04 anexo = new Anexo04();
        anexo.setCabecalho(new Cabecalho(1, 30, 1, 2014, "12345", 2));

        writer.writeAndFlush(anexo);

        String cabecalho = stream.toString().substring(0, 34);
        assertThat(cabecalho, is("****08013001201400000123450002X04 "));
    }

    @Test
    public void escreveCreditosParaDoisPeriodos() throws IOException, ParseException {
        Anexo04 anexo = new Anexo04();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        anexo.adicionarDetalhe(new DetalheAnexo04(1, 30, 1, 2014, sdf.parse("20/02/2014"), 0D, 0D));
        anexo.adicionarDetalhe(new DetalheAnexo04(1, 28, 2, 2014, sdf.parse("20/03/2014"), 0D, 0D));

        writer.writeAndFlush(anexo);

        String conteudo = stream.toString().substring(34, 124);
        assertThat(conteudo, is("020130012014200220140000000000000000000000000001280220142003201400000000000000000000000000"));
    }
}
