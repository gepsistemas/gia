package br.com.gep.gia.marshaller.writer;

import org.junit.After;
import org.junit.Before;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class BaseWriteTest {

    protected GiaWriter writer;
    protected ByteArrayOutputStream stream;

    @Before
    public void inicializacao() throws UnsupportedEncodingException {
        stream = new ByteArrayOutputStream();
        writer = new GiaWriter(new OutputStreamWriter(stream, "ISO-8859-1"));
    }

    @After
    public void liberacaoRecursos() throws IOException {
        if (writer != null) {
            writer.flush();
            writer.close();
        }
    }
}
