package br.com.gep.gia.batch.reader;

import br.com.gep.gia.batch.SchemaAwareTest;
import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.registros.detalhes.QuadroB;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AnexosReadTest extends SchemaAwareTest {

    @Test
    public void buscaQuadroBPorCnpj() throws Exception {
        ItemReader<QuadroB> reader = itemReaderFactory.create(QuadroB.class);

        QuadroB quadro =  reader.read();

        assertThat(quadro).isNotNull();
    }

    @Test
    public void buscaAnexo01PorCnpj() throws Exception {
        ItemReader<Anexo01> reader = itemReaderFactory.create(Anexo01.class);

        Anexo01 anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(13);
    }

    @Test
    public void buscaAnexo01CPorCnpj() throws Exception {
        ItemReader<Anexo01C> reader = itemReaderFactory.create(Anexo01C.class);

        Anexo01C anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(13);
    }

    @Test
    public void buscaAnexo02PorCnpj() throws Exception {
        ItemReader<Anexo02> reader = itemReaderFactory.create(Anexo02.class);

        Anexo02 anexo =  reader.read();

        assertThat(anexo).isNull();
    }

    @Test
    public void buscaAnexo05PorCnpj() throws Exception {
        ItemReader<Anexo05> reader = itemReaderFactory.create(Anexo05.class);

        Anexo05 anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(15);
    }

    @Test
    public void buscaAnexo05APorCnpj() throws Exception {
        ItemReader<Anexo05A> reader = itemReaderFactory.create(Anexo05A.class);

        Anexo05A anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(6);
    }

    @Test
    public void buscaAnexo05BPorCnpj() throws Exception {
        ItemReader<Anexo05B> reader = itemReaderFactory.create(Anexo05B.class);

        Anexo05B anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(7);
    }

    @Test
    public void buscaAnexo05CPorCnpj() throws Exception {
        ItemReader<Anexo05C> reader = itemReaderFactory.create(Anexo05C.class);

        Anexo05C anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(15);
    }

    @Test
    public void buscaAnexo06PorCnpj() throws Exception {
        ItemReader<Anexo06> reader = itemReaderFactory.create(Anexo06.class);

        Anexo06 anexo =  reader.read();

        assertThat(anexo).isNull();
    }

    @Test
    public void buscaAnexo07PorCnpj() throws Exception {
        ItemReader<Anexo07> reader = itemReaderFactory.create(Anexo07.class);

        Anexo07 anexo07 = reader.read();

        assertThat(anexo07).isNotNull();
    }

    @Test
    public void buscaAnexo07APorCnpj() throws Exception {
        ItemReader<Anexo07A> reader = itemReaderFactory.create(Anexo07A.class);

        Anexo07A anexo =  reader.read();

        assertThat(anexo).isNull();
    }

    @Test
    public void buscaAnexo07BPorCnpj() throws Exception {
        ItemReader<Anexo07B> reader = itemReaderFactory.create(Anexo07B.class);

        Anexo07B anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(1);
    }

    @Test
    public void buscaAnexo10PorCnpj() throws Exception {
        ItemReader<Anexo10> reader = itemReaderFactory.create(Anexo10.class);

        Anexo10 anexo =  reader.read();

        assertThat(anexo).isNotNull();
        assertThat(anexo.getDetalhes()).hasSize(1);
    }

    @Test
    public void buscaAnexo14PorCnpj() throws Exception {
        ItemReader<Anexo14> reader = itemReaderFactory.create(Anexo14.class);

        Anexo14 anexo =  reader.read();

        assertThat(anexo.getDetalhes()).hasSize(3);
    }

    @Test
    public void buscaAnexo15PorCnpj() throws Exception {
        ItemReader<Anexo15> reader = itemReaderFactory.create(Anexo15.class);

        Anexo15 anexo =  reader.read();

        assertThat(anexo.getDetalhes()).hasSize(1);
    }
}
