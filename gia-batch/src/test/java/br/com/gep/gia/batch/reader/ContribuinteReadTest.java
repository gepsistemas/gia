package br.com.gep.gia.batch.reader;

import br.com.gep.gia.batch.SchemaAwareTest;
import br.com.gep.gia.marshaller.registros.Contribuinte;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class ContribuinteReadTest extends SchemaAwareTest {

    @Test
    public void buscaContribuintePorCnpj() throws Exception {
        ItemReader<Contribuinte> reader = itemReaderFactory.create(Contribuinte.class);
        Contribuinte contribuinte = reader.read();

        assertThat(contribuinte).isNotNull();
        assertThat(contribuinte.getCnpj()).isEqualTo("94296175000131");
    }
}
