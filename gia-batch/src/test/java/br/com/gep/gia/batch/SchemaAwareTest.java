package br.com.gep.gia.batch;

import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.HashMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public abstract class SchemaAwareTest {

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected ItemReaderFactory itemReaderFactory;

    protected String schema = "gia_teste";

    @Before
    public void before() {
        // sintaxe do HSQLDB
        jdbcTemplate.execute("set schema " + schema);

        HashMap<String, Object> params = new HashMap<>();
        params.put("cnpj", "94296175000131");
        params.put("config", new GiaConfig());
        itemReaderFactory.setParams(params);
    }
}
