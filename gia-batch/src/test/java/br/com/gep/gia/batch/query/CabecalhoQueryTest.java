package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.SchemaAwareTest;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class CabecalhoQueryTest extends SchemaAwareTest {

    @Test
    public void buscaCabecalhoPorCnpj() {
        CabecalhoQuery query = new CabecalhoQuery();
        query.setDataSource(dataSource);

        Cabecalho cabecalho = query.execute("94296175000131");

        assertThat(cabecalho).isNotNull();
        assertThat(cabecalho.getMes()).isEqualTo(6);
        assertThat(cabecalho.getAno()).isEqualTo(2014);
    }
}
