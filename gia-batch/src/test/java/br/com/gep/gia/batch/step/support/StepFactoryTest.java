package br.com.gep.gia.batch.step.support;

import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import br.com.gep.gia.batch.step.SimpleStep;
import br.com.gep.gia.batch.step.Step;
import br.com.gep.gia.marshaller.registros.Anexo07A;
import br.com.gep.gia.marshaller.registros.Contribuinte;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class StepFactoryTest {

    private DataSource dataSource;
    private StepFactory factory;

    @Before
    public void before() {
        dataSource = mock(DataSource.class);
        factory = new StepFactory();
        ItemReaderFactory readerFactory = new ItemReaderFactory(dataSource);
        readerFactory.setParams(new HashMap<String, Object>());
        factory.setReaderFactory(readerFactory);
    }

    @Test
    public void criaInstanciaDeContribuinteStepCorretamente() {
        try {
            Step step = factory.create(Contribuinte.class);

            assertThat(step).isInstanceOf(SimpleStep.class);
            assertThat(((SimpleStep) step).getReader()).isNotNull();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            fail("não deveria lançar exceção");
        }
    }

    @Test
    public void criaInstanciaDeAnexo07AStepCorretamente() {
        try {
            Step step = factory.create(Anexo07A.class);

            assertThat(step).isInstanceOf(SimpleStep.class);
            assertThat(((SimpleStep) step).getReader()).isNotNull();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            fail("não deveria lançar exceção");
        }
    }
}
