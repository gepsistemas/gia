package br.com.gep.gia.batch.step;

import br.com.gep.gia.batch.GiaConfig;
import br.com.gep.gia.batch.reader.ItemReader;
import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import br.com.gep.gia.batch.step.support.StepFactory;
import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.registros.detalhes.QuadroB;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
public class StepsTest {

    private ItemReader reader;
    private StepFactory stepFactory;
    private Estrutura estrutura;
    private Map params;
    private final String cnpj = "00000000000000";

    @Before
    public void before() {
        estrutura = new Estrutura(new Cabecalho());
        params = new HashMap() {{ put("cnpj", cnpj); put("config", new GiaConfig()); }};

        reader = mock(ItemReader.class);

        ItemReaderFactory readerFactory = mock(ItemReaderFactory.class);
        readerFactory.setParams(params);
        when(readerFactory.create(any(Class.class))).thenReturn(reader);

        stepFactory = new StepFactory();
        stepFactory.setReaderFactory(readerFactory);
    }

    @Test
    public void populaEstruturaComContribuinte() throws Exception {
        Contribuinte contribuinte = new Contribuinte();
        when(reader.read()).thenReturn(contribuinte);

        Step step = stepFactory.create(Contribuinte.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getContribuinte()).isEqualTo(contribuinte);
    }

    @Test
    public void populaEstruturaComQuadroB() throws Exception {
        QuadroB quadro = new QuadroB();
        when(reader.read()).thenReturn(quadro);

        Step step = stepFactory.create(QuadroB.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getPrincipal().getQuadroB()).isEqualTo(quadro);
    }

    @Test
    public void populaEstruturaComAnexo01() throws Exception {
        Anexo01 anexo = new Anexo01();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo01.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo01()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo01C() throws Exception {
        Anexo01C anexo = new Anexo01C();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo01C.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo01C()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo02() throws Exception {
        Anexo02 anexo = new Anexo02();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo02.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo02()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo05() throws Exception {
        Anexo05 anexo = new Anexo05();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo05.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo05()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo05A() throws Exception {
        Anexo05A anexo = new Anexo05A();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo05A.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo05A()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo05B() throws Exception {
        Anexo05B anexo = new Anexo05B();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo05B.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo05B()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo05C() throws Exception {
        Anexo05C anexo = new Anexo05C();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo05C.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo05C()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo06() throws Exception {
        Anexo06 anexo = new Anexo06();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo06.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo06()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo07() throws Exception {
        Anexo07 anexo = new Anexo07();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo07.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo07()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo07A() throws Exception {
        Anexo07A anexo = new Anexo07A();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo07A.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo07A()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo07B() throws Exception {
        Anexo07B anexo = new Anexo07B();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo07B.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo07B()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo10() throws Exception {
        Anexo10 anexo = new Anexo10();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo10.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo10()).isEqualTo(anexo);
    }

    @Test
    public void populaEstruturaComAnexo14() throws Exception {
        Anexo14 anexo = new Anexo14();
        when(reader.read()).thenReturn(anexo);

        Step step = stepFactory.create(Anexo14.class);
        step.execute(estrutura, params);

        assertThat(estrutura.getAnexo14()).isEqualTo(anexo);
    }

    @Test
    public void configuraEstrutura() {
        GiaConfig config = new GiaConfig();
        config.setRetificadora(true);
        config.setNumeroEmpregados(300);
        config.setInicioAtividade(true);
        config.getQuadroE().setEstoqueInicialTributado(100);
        config.getQuadroE().setEstoqueInicialIsento(100);
        config.getQuadroE().setEstoqueInicialComTerceiros(100);
        config.getQuadroE().setEstoqueInicialDeTerceiros(100);
        params.put("config", config);

        new ConfigStep().execute(estrutura, params);

        Principal principal = estrutura.getPrincipal();
        assertThat(principal.isRetificacao()).isTrue();
        assertThat(principal.getQuadroC().getNumeroEmpregados()).isEqualTo(300);
        assertThat(principal.getTotais().isInicioAtividade()).isTrue();
        assertThat(principal.getQuadroE()).isEqualTo(config.getQuadroE());
    }

    @Test
    public void escreveArquivo() throws Exception {
        String caminho = "target/gia_teste.txt";
        params.put("outputFile", caminho);

        new WriteFileStep().execute(estrutura, params);

        File arquivo = new File(caminho);
        assertThat(arquivo).exists();
        assertThat(arquivo.length()).isGreaterThan(0);
    }

    @Test
    public void naoLancaExcecaoSeReaderRetornarNull() throws Exception {
        when(reader.read()).thenReturn(null);

        SimpleStep step = new SimpleStep();
        step.setReader(reader);

        try {
            step.execute(estrutura, params);
        }
        catch (Exception e) {
            fail("Exceção não deveria ser lançada", e);
        }
    }
}
