package br.com.gep.gia.batch.query.support;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QueriesBundleTest {

    @Test
    public void seSchemaNaoInformadoObtemSqlSemTokens() {
        QueriesBundle.setSchema("");
        String sql = QueriesBundle.getSql("Teste");

        assertThat(sql.trim()).isEqualTo("select * from tabela1 join tabela2");
    }

    @Test
    public void seSchemaInformadoSubstituiTokensPorSchema() {
        QueriesBundle.setSchema("teste");
        String sql = QueriesBundle.getSql("Teste");

        assertThat(sql.trim()).isEqualTo("select * from teste.tabela1 join teste.tabela2");
    }
}
