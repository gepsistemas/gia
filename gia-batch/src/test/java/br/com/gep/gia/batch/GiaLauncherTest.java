package br.com.gep.gia.batch;

import br.com.gep.gia.batch.execution.ExecucaoGia;
import br.com.gep.gia.batch.execution.ExecucaoGiaDao;
import br.com.gep.gia.batch.execution.StatusExecucao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.File;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GiaLauncherTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private ExecucaoGiaDao execucaoDao;

    private GiaLauncher launcher;

    @Before
    public void before() throws Exception {
        launcher = new GiaLauncher(dataSource);
        launcher.setSchema("gia_teste");
        launcher.setDestinationDir("target");
    }

    @Test
    public void executaSincronamenteSemErros() throws Exception {
        ExecucaoGia execucao = launcher.run("94296175000131", new GiaConfig());

        File arquivo = new File("target", "GIA_RS_94296175000131_2014_06.txt");
        assertThat(arquivo).exists();
        assertThat(execucao.getStatus()).isEqualTo(StatusExecucao.COMPLETO);
    }

    @Test
    public void executaDuasVezesAssincronamenteSemErros() throws Exception {
        launcher.setTaskExecutor(new SimpleAsyncTaskExecutor());

        ExecucaoGia execucao1 = launcher.run("94296175000131", new GiaConfig(), "target/gia_teste1.txt");
        ExecucaoGia execucao2 = launcher.run("94296175000231", new GiaConfig(), "target/gia_teste2.txt");

        while (!execucaoCompleta(execucao1) || !execucaoCompleta(execucao2)) {
            Thread.sleep(500);
        }

        assertThat(execucao1.getStatus()).isEqualTo(StatusExecucao.COMPLETO);
        assertThat(execucao2.getStatus()).isEqualTo(StatusExecucao.COMPLETO);
        File arquivo1 = new File("target", "gia_teste1.txt");
        File arquivo2 = new File("target", "gia_teste2.txt");
        assertThat(arquivo1).exists();
        assertThat(arquivo2).exists();
    }

    @Test
    public void lancaExcecaoSeArquivoEstiverSendoGerado() throws Exception {
        launcher.setTaskExecutor(new SimpleAsyncTaskExecutor());

        ExecucaoGia execucao = launcher.run("94296175000131", new GiaConfig());

        try {
            launcher.run("94296175000131", new GiaConfig());
            failBecauseExceptionWasNotThrown(ExecucaoJaFoiIniciadaException.class);
        }
        catch (ExecucaoJaFoiIniciadaException e) {}

        while (!execucaoCompleta(execucao)) {
            Thread.sleep(500);
        }
    }

    @Test
    public void lancaExcecaoSeArquivoJaFoiGerado() throws Exception {
        ExecucaoGia execucao = launcher.run("94296175000131", new GiaConfig());

        try {
            launcher.run("94296175000131", new GiaConfig());
            failBecauseExceptionWasNotThrown(ExecucaoJaEstaCompletaException.class);
        }
        catch (ExecucaoJaEstaCompletaException e) {}

        while (!execucaoCompleta(execucao)) {
            Thread.sleep(500);
        }
    }

    private boolean execucaoCompleta(ExecucaoGia execucao) {
        return execucaoDao.buscar(execucao.getId()).getStatus() == StatusExecucao.COMPLETO;
    }
}
