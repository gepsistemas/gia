package br.com.gep.gia.batch;

import br.com.gep.gia.batch.execution.ExecucaoGiaDao;
import br.com.gep.gia.batch.execution.jdbc.JdbcExecucaoGiaDao;
import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@ComponentScan
@PropertySource(value = "classpath:jdbc.properties", ignoreResourceNotFound = true)
public class TestConfig {

    @Autowired
    private Environment env;

    @Bean @Primary
    public DataSource dataSourceHSQL() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("classpath:db/schema-hsqldb.sql")
                .addScripts("classpath:execucoes_teste.sql")
                .addScripts("classpath:assinaturas_teste.sql")
                .addScripts("classpath:gia_teste.sql")
                .build();
    }

    @Bean @Lazy
    public DataSource dataSourcePgSQL() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(env.getProperty("jdbc.url", "jdbc:postgresql://localhost:5432/foo"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.password"));

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManagerHSQL() {
        return new DataSourceTransactionManager(dataSourceHSQL());
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSourceHSQL());
    }

    @Bean
    public ExecucaoGiaDao execucaoGiaDao() {
        return new JdbcExecucaoGiaDao(dataSourceHSQL());
    }

    @Bean
    public ItemReaderFactory itemReaderFactory() {
        return new ItemReaderFactory(dataSourceHSQL());
    }
}
