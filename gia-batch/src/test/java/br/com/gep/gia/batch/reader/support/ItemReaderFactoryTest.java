package br.com.gep.gia.batch.reader.support;

import br.com.gep.gia.batch.reader.Anexo07ItemReader;
import br.com.gep.gia.batch.reader.AnexoItemReader;
import br.com.gep.gia.batch.reader.ItemReader;
import br.com.gep.gia.batch.reader.JdbcItemReader;
import br.com.gep.gia.marshaller.registros.Anexo01;
import br.com.gep.gia.marshaller.registros.Anexo07;
import br.com.gep.gia.marshaller.registros.Contribuinte;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

public class ItemReaderFactoryTest {

    private ItemReaderFactory factory;

    @Before
    public void setup() {
        factory = new ItemReaderFactory(new DriverManagerDataSource());
        factory.setParams(new HashMap<String, Object>());
    }

    @Test
    public void criaJdbcItemReader() {
        ItemReader<Contribuinte> reader = factory.create(Contribuinte.class);

        assertThat(reader).isInstanceOf(JdbcItemReader.class);
    }

    @Test
    public void criaItemReaderEspecializado() {
        ItemReader<Anexo07> reader = factory.create(Anexo07.class);

        assertThat(reader).isInstanceOf(Anexo07ItemReader.class);
    }

    @Test
    public void criaAnexoComDetalheItemReader() {
        ItemReader<Anexo01> reader = factory.create(Anexo01.class);

        assertThat(reader).isInstanceOf(AnexoItemReader.class);
    }
}
