package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.SchemaAwareTest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class ClienteQueryTest extends SchemaAwareTest {

    @Test
    public void buscaCliente() {
        ClienteQuery query = new ClienteQuery();
        query.setDataSource(dataSource);

        String cliente = query.execute();

        assertThat(cliente).isEqualTo("94296175000131");
    }
}
