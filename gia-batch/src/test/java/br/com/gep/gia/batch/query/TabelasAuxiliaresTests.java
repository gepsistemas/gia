package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.TestConfig;
import br.com.gep.gia.batch.query.support.QueriesBundle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class TabelasAuxiliaresTests {

    public static final String SCHEMA = "carga_94296175000131_2015_01";

    @Autowired
    private TestConfig config;

    private JdbcTemplate jdbcTemplate;
    private TabelasAuxiliares tabelasAuxiliares;

    @Before
    public void setup() {
        jdbcTemplate = new JdbcTemplate(config.dataSourcePgSQL());
        tabelasAuxiliares = new TabelasAuxiliares(config.dataSourcePgSQL());
        QueriesBundle.setSchema(SCHEMA);

        execute("DROP TABLE IF EXISTS base_gia");
        execute("DROP TABLE IF EXISTS base_gia_saidas_isentas");
    }

    private void setSchema() {
        jdbcTemplate.execute("set search_path to " + SCHEMA);
    }

    private void execute(String sql) {
        setSchema();
        jdbcTemplate.execute(sql);
    }

    private boolean getBoolean(String sql) {
        setSchema();
        return jdbcTemplate.queryForObject(sql, Boolean.class);
    }

    @Test
    public void verificaSeTabelaBaseExiste() {
        assertThat(tabelasAuxiliares.tabelaBaseExiste()).isFalse();

        execute("CREATE TABLE base_gia (id int not null)");

        assertThat(tabelasAuxiliares.tabelaBaseExiste()).isTrue();
    }

    @Test
    public void criaTabelaBaseSeNaoExistir() {
        tabelasAuxiliares.criar();

        assertThat(tabelasAuxiliares.tabelaBaseExiste()).isTrue();
    }

    @Test
    public void trataCfop1949() {
        tabelasAuxiliares.criar();

        String sql = "select count(0) = 0 from base_gia where cfop = '1949' and valor_contabil = 0 and bc_icms <> 0";
        boolean tratado = getBoolean(sql);

        assertThat(tratado).isTrue();
    }

    @Test
    public void atualizaColunaIsentasDasEntradas() {
        tabelasAuxiliares.criar(true, true);

        boolean ätualizada = getBoolean("select count(0) > 0 from base_gia where isentas > 0 and ent_sai = 'E'");

        assertThat(ätualizada).isTrue();
    }

    @Test
    public void atualizaColunaOutrasDasEntradas() {
        tabelasAuxiliares.criar();

        boolean atualizada = getBoolean("select count(0) > 0 from base_gia where outras > 0 and ent_sai = 'E'");

        assertThat(atualizada).isTrue();
    }

    @Test
    public void atualizaColunaIsentasComReducaoBaseDasEntradas() {
        tabelasAuxiliares.criar();

        boolean atualizado = getBoolean("select count(0) > 0 from base_gia where isentas = vl_red_bc and ent_sai = 'E'");

        assertThat(atualizado).isTrue();
    }

    @Test
    public void trataCst41Cfop5102() {
        tabelasAuxiliares.criar();

        String sql = "select count(0) = 0 from base_gia where cfop = '5102' and cst2 = '41'";
        boolean tratado = getBoolean(sql);

        assertThat(tratado).isTrue();
    }

    @Test
    public void trataCst41Cfops5152e5949() {
        tabelasAuxiliares.criar();

        String sql = "select count(0) = 0 from base_gia where cfop in ('5152', '5949') and cst2 = '41'";
        boolean tratado = getBoolean(sql);

        assertThat(tratado).isTrue();
    }

    @Test
    public void atualizaColunaIsentasDasSaidas() {
        tabelasAuxiliares.criar();

        boolean atualizada = getBoolean("select count(0) > 0 from base_gia where isentas > 0 and ent_sai = 'S'");

        assertThat(atualizada).isTrue();
    }

    @Test
    public void atualizaColunaOutrasDasSaidas() {
        tabelasAuxiliares.criar();

        boolean atualizada = getBoolean("select count(0) > 0 from base_gia where outras > 0 and ent_sai = 'S'");

        assertThat(atualizada).isTrue();
    }

    @Test
    public void atualizaColunaIsentasComReducaoBaseDasSaidas() {
        tabelasAuxiliares.criar();

        boolean atualizado = getBoolean("select count(0) > 0 from base_gia where isentas = vl_red_bc and ent_sai = 'S'");

        assertThat(atualizado).isTrue();
    }

    @Test
    public void trataCfop6409() {
        tabelasAuxiliares.criar();

        String sql = "select count(0) = 0 from base_gia where cfop = '6409' and valor_contabil > bc_icms and outras = 0";
        boolean tratado = getBoolean(sql);

        assertThat(tratado).isTrue();
    }

    @Test
    public void trataCfops5603e6603() {
        tabelasAuxiliares.criar();

        String sql = "select count(0) = 0 from base_gia where cfop in ('5603', '6603') and outras <> valor_contabil";
        boolean tratado = getBoolean(sql);

        assertThat(tratado).isTrue();
    }

    @Test
    public void atualizaColunaExcluidasComSomatorioColunas() {
        tabelasAuxiliares.criar();

        boolean atualizado = getBoolean("select count(0) > 0 from base_gia where excluidas = bc_icms + isentas + outras");

        assertThat(atualizado).isTrue();
    }

    @Test
    public void atualizaColunaExcluidasComStIpiNasEntradasQuandoExisteAnexo01C() {
        tabelasAuxiliares.criar(true, false);

        boolean atualizado =
                getBoolean("select count(0) > 0 from base_gia where excluidas = vl_ipi + vl_icms_st and ent_sai = 'E'");

        assertThat(atualizado).isTrue();
    }

    @Test
    public void atualizaColunaExcluidasComStIpiNasSaidasQuandoExisteAnexo05C() {
        tabelasAuxiliares.criar(false, true);

        boolean atualizado =
                getBoolean("select count(0) > 0 from base_gia where excluidas = vl_ipi + vl_icms_st and ent_sai = 'S'");

        assertThat(atualizado).isTrue();
    }

    @Test
    public void verificaSeTabelaSaidasIsentasExiste() {
        assertThat(tabelasAuxiliares.tabelaSaidasIsentasExiste()).isFalse();

        execute("CREATE TABLE base_gia_saidas_isentas (id int not null)");

        assertThat(tabelasAuxiliares.tabelaSaidasIsentasExiste()).isTrue();
    }

    @Test
    public void criaTabelaSaidaIsentasSeNaoExistir() {
        tabelasAuxiliares.criar();

        assertThat(tabelasAuxiliares.tabelaSaidasIsentasExiste()).isTrue();
    }
}
