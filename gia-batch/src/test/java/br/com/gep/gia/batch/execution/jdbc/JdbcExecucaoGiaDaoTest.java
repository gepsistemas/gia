package br.com.gep.gia.batch.execution.jdbc;

import br.com.gep.gia.batch.TestConfig;
import br.com.gep.gia.batch.execution.ExecucaoGia;
import br.com.gep.gia.batch.execution.StatusExecucao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcExecucaoGiaDaoTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private JdbcExecucaoGiaDao dao;

    @Before
    public void before() {
        dao = new JdbcExecucaoGiaDao(dataSource);
    }

    @Test
    public void criaNovaExecucao() {
        ExecucaoGia execucao = new ExecucaoGia();
        execucao.setCnpjCliente("00000000000000");
        execucao.setCnpjEstabelecimento("11111111111111");
        execucao.setNome("Teste");
        execucao.setAno(2014);
        execucao.setMes(1);
        execucao.setArquivo("arquivo.txt");
        execucao.setInicio(new Date());

        dao.criar(execucao);

        assertThat(execucao.getId()).isNotNull();
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, JdbcExecucaoGiaDao.TABELA);
        assertThat(count).isEqualTo(3);
    }

    @Test
    public void buscaExecucaoPorId() {
        ExecucaoGia execucao = dao.buscar(1);

        assertThat(execucao).isNotNull();
    }

    @Test
    public void buscaExecucaoPorEstabelecimentoAnoMes() {
        ExecucaoGia execucao = dao.buscar("11111111111111", 2014, 1);

        assertThat(execucao).isNotNull();
    }

    @Test
    public void buscaPorEstabelecimentoAnoMesRetornaNullSeNaoTiverResultados() {
        ExecucaoGia execucao = dao.buscar("invalido", 2014, 1);

        assertThat(execucao).isNull();
    }

    @Test
    public void atualizaExecucaoExistente() throws ParseException {
        ExecucaoGia execucao = dao.buscar("22222222222222", 2014, 2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date termino = sdf.parse("01/03/2014 12:01:00");
        execucao.setTermino(termino);
        execucao.setStatus(StatusExecucao.COMPLETO);

        dao.atualizar(execucao);

        ExecucaoGia execucaoAlterada = dao.buscar("22222222222222", 2014, 2);
        assertThat(execucaoAlterada.getTermino()).isCloseTo(termino, 1);
        assertThat(execucaoAlterada.getStatus()).isEqualTo(StatusExecucao.COMPLETO);
    }

    @Test
    public void listaExecucoesPorCliente() {
        List<ExecucaoGia> itens = dao.listarPorCliente("11111111111111");

        assertThat(itens).hasSize(1);
    }

    @Test
    public void listaExecucoesPorEstabelecimento() {
        List<ExecucaoGia> itens = dao.listarPorEstabelecimento("22222222222222");

        assertThat(itens).hasSize(1);
    }
}
