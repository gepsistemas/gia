package br.com.gep.gia.batch;

public class ExecucaoJaEstaCompletaException extends Exception {

    public ExecucaoJaEstaCompletaException() {
    }

    public ExecucaoJaEstaCompletaException(String message) {
        super(message);
    }

    public ExecucaoJaEstaCompletaException(String message, Throwable cause) {
        super(message, cause);
    }
}
