package br.com.gep.gia.batch.step;

import br.com.gep.gia.marshaller.registros.Estrutura;
import br.com.gep.gia.marshaller.registros.Observacoes;

import java.util.Map;

public class ObservacoesStep implements Step {

    @Override
    public void execute(Estrutura context, Map parameters) throws Exception {
        Observacoes obs = new Observacoes();
        obs.setTexto("OUTRAS ENTRADAS/SAIDAS");
        context.setObservacoes(obs);
    }
}
