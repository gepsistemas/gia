package br.com.gep.gia.batch.reader;

import br.com.gep.gia.marshaller.registros.AnexoComDetalhe;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnexoItemReader<TAnexo extends AnexoComDetalhe, TDetalhe> extends JdbcItemReader<TAnexo> {

    private final Class<TAnexo> anexoClass;
    private final Class<TDetalhe> detalheClass;

    public AnexoItemReader(Class<TAnexo> anexoClass, Class<TDetalhe> detalheClass) {
        this.anexoClass = anexoClass;
        this.detalheClass = detalheClass;
    }

    @Override
    public TAnexo read() throws Exception {
        final TAnexo anexo = anexoClass.newInstance();

        RowMapper<TDetalhe> rowMapper = new BeanPropertyRowMapper<TDetalhe>(detalheClass) {
            @SuppressWarnings("unchecked")
            @Override
            public TDetalhe mapRow(ResultSet rs, int rowNum) throws SQLException {
                TDetalhe detalhe = super.mapRow(rs, rowNum);
                anexo.adicionarDetalhe(detalhe);
                return detalhe;
            }
        };

        jdbcTemplate.query(sql, params, rowMapper);

        return anexo.getOcorrencias() > 0 ? anexo : null;
    }
}
