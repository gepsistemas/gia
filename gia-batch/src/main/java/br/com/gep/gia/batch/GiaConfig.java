package br.com.gep.gia.batch;

import br.com.gep.gia.marshaller.registros.detalhes.QuadroE;

public class GiaConfig {

    private boolean retificadora;
    private int numeroEmpregados;
    private double valorFolhaPagamento;
    private boolean transportaSaldoDevedorIcmsProprioProximoMes;
    private boolean inicioAtividade;
    private boolean fimAtividade;
    private QuadroE quadroE;
    private boolean anexo01CIncluso = false, anexo05CIncluso = false;

    public GiaConfig() {
        quadroE = new QuadroE();
    }

    public boolean isRetificadora() {
        return retificadora;
    }

    public void setRetificadora(boolean retificadora) {
        this.retificadora = retificadora;
    }

    public int getNumeroEmpregados() {
        return numeroEmpregados;
    }

    public void setNumeroEmpregados(int numeroEmpregados) {
        this.numeroEmpregados = numeroEmpregados;
    }

    public double getValorFolhaPagamento() {
        return valorFolhaPagamento;
    }

    public void setValorFolhaPagamento(double valorFolhaPagamento) {
        this.valorFolhaPagamento = valorFolhaPagamento;
    }

    public boolean isTransportaSaldoDevedorIcmsProprioProximoMes() {
        return transportaSaldoDevedorIcmsProprioProximoMes;
    }

    public void setTransportaSaldoDevedorIcmsProprioProximoMes(boolean transportaSaldoDevedorIcmsProprioProximoMes) {
        this.transportaSaldoDevedorIcmsProprioProximoMes = transportaSaldoDevedorIcmsProprioProximoMes;
    }

    public boolean isInicioAtividade() {
        return inicioAtividade;
    }

    public void setInicioAtividade(boolean inicioAtividade) {
        this.inicioAtividade = inicioAtividade;
    }

    public boolean isFimAtividade() {
        return fimAtividade;
    }

    public void setFimAtividade(boolean fimAtividade) {
        this.fimAtividade = fimAtividade;
    }

    public QuadroE getQuadroE() {
        return quadroE;
    }

    public void setQuadroE(QuadroE quadroE) {
        this.quadroE = quadroE;
    }

    public boolean isAnexo01CIncluso() {
        return anexo01CIncluso;
    }

    void setAnexo01CIncluso(boolean anexo01CIncluso) {
        this.anexo01CIncluso = anexo01CIncluso;
    }

    public boolean isAnexo05CIncluso() {
        return anexo05CIncluso;
    }

    void setAnexo05CIncluso(boolean anexo05CIncluso) {
        this.anexo05CIncluso = anexo05CIncluso;
    }
}
