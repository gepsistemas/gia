package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.query.support.QueriesBundle;
import org.springframework.dao.EmptyResultDataAccessException;

public class ClienteQuery extends AbstractQuery<String> {

    /**
     * @param parameters Não utilizado
     */
    @Override
    public String execute(Object... parameters) {
        String sql = QueriesBundle.getSql("Cliente");

        try {
            return jdbcTemplate.queryForObject(sql,
                    String.class,
                    parameters);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
