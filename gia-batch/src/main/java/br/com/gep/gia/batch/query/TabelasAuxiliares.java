package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.query.support.QueriesBundle;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TabelasAuxiliares {

    private JdbcTemplate jdbcTemplate;
    private PlatformTransactionManager txManager;

    public TabelasAuxiliares() {
    }

    public TabelasAuxiliares(DataSource dataSource) {
        setDataSource(dataSource);
    }

    public void setDataSource(DataSource dataSource) {
        Assert.notNull(dataSource, "dataSource nao deve ser nulo");

        jdbcTemplate = new JdbcTemplate(dataSource);
        txManager = new DataSourceTransactionManager(dataSource);
    }

    public void criar() {
        criar(true, true);
    }

    public void criar(final boolean anexo01C, final boolean anexo05C) {
        if (getDatabaseName().startsWith("HSQL")) return; // para testes

        if (tabelaBaseExiste()) return;

        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(TransactionStatus status) {
                jdbcTemplate.execute("set schema '" + QueriesBundle.getSchema() + "'");

                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("anexo01C", anexo01C);
                params.addValue("anexo05C", anexo05C);
                String sqlTabelaBase = QueriesBundle.getSql("CriarTabelaBase", params);
                jdbcTemplate.execute(sqlTabelaBase);

                if (!tabelaSaidasIsentasExiste()) {
                    jdbcTemplate.execute(getSql("CriarTabelaSaidasIsentas"));
                }

                return null;
            }
        });
    }

    private String getDatabaseName() {
        try {
            return JdbcUtils
                    .extractDatabaseMetaData(jdbcTemplate.getDataSource(), "getDatabaseProductName")
                    .toString();
        } catch (MetaDataAccessException e) {
            return "";
        }
    }

    public boolean tabelaBaseExiste() {
        return jdbcTemplate.queryForObject(
                getSql("TabelaBaseExiste"), Boolean.class, QueriesBundle.getSchema());
    }

    public boolean tabelaSaidasIsentasExiste() {
        return jdbcTemplate.queryForObject(
                getSql("TabelaSaidaIsentasExiste"), Boolean.class, QueriesBundle.getSchema());
    }

    private String getSql(String name) {
        return QueriesBundle.getSql(name);
    }
}
