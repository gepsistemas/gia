package br.com.gep.gia.batch.reader;

public interface ItemReader<T> {

    T read() throws Exception;
}
