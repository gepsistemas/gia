package br.com.gep.gia.batch.query;

import br.com.gep.gia.batch.query.support.QueriesBundle;
import br.com.gep.gia.marshaller.registros.Cabecalho;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

public class CabecalhoQuery extends AbstractQuery<Cabecalho> {

    /**
     * @param parameters Informar o CNPJ
     */
    @Override
    public Cabecalho execute(Object... parameters) {
        String sql = QueriesBundle.getSql("Cabecalho");

        try {
            return jdbcTemplate.queryForObject(sql,
                    new BeanPropertyRowMapper<>(Cabecalho.class),
                    parameters);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
