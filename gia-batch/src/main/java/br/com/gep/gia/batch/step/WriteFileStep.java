package br.com.gep.gia.batch.step;

import br.com.gep.gia.marshaller.registros.Estrutura;
import br.com.gep.gia.marshaller.writer.GiaWriter;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class WriteFileStep implements Step {

    @Override
    public void execute(Estrutura context, Map parameters) throws Exception {
        String outputFile = parameters.get("outputFile").toString();
        Files.deleteIfExists(Paths.get(outputFile));
        Files.createFile(Paths.get(outputFile));

        try (GiaWriter writer = new GiaWriter(new File(outputFile))) {
            writer.writeAndFlush(context);
        } catch (Exception e) {
            throw e;
        }
    }
}
