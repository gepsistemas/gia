package br.com.gep.gia.batch;

public class ExecucaoJaFoiIniciadaException extends Exception {

    public ExecucaoJaFoiIniciadaException() {
    }

    public ExecucaoJaFoiIniciadaException(String message) {
        super(message);
    }

    public ExecucaoJaFoiIniciadaException(String message, Throwable cause) {
        super(message, cause);
    }
}
