package br.com.gep.gia.batch.reader;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;

public class JdbcItemReader<T> implements ItemReader<T> {

    protected NamedParameterJdbcTemplate jdbcTemplate;
    protected String sql;
    protected SqlParameterSource params;
    protected RowMapper<T> rowMapper;

    @Override
    public T read() throws Exception{
        try {
            return jdbcTemplate.queryForObject(sql, params, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

    }

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public void setParams(SqlParameterSource params) {
        this.params = params;
    }

    public void setRowMapper(RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;
    }
}
