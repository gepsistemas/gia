package br.com.gep.gia.batch.reader;

public class NoOpItemReader implements ItemReader {

    @Override
    public Object read() {
        return null;
    }
}
