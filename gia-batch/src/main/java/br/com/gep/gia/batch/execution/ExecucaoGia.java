package br.com.gep.gia.batch.execution;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

public class ExecucaoGia {

    private Long id;
    private String cnpjCliente, cnpjEstabelecimento, nome;
    private int ano, mes;
    private String arquivo;
    private Date inicio, termino;
    private StatusExecucao status = StatusExecucao.INICIADO;
    private String mensagemSaida;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpjCliente() {
        return cnpjCliente;
    }

    public void setCnpjCliente(String cnpjCliente) {
        this.cnpjCliente = cnpjCliente;
    }

    public String getCnpjEstabelecimento() {
        return cnpjEstabelecimento;
    }

    public void setCnpjEstabelecimento(String cnpjEstabelecimento) {
        this.cnpjEstabelecimento = cnpjEstabelecimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getTermino() {
        return termino;
    }

    public void setTermino(Date termino) {
        this.termino = termino;
    }

    public StatusExecucao getStatus() {
        return status;
    }

    public void setStatus(StatusExecucao status) {
        this.status = status;
    }

    public String getMensagemSaida() {
        return mensagemSaida;
    }

    public void setMensagemSaida(String mensagemSaida) {
        this.mensagemSaida = mensagemSaida;
    }

    public void setMensagemSaida(Throwable throwable) {
        StringWriter writer = new StringWriter();
        throwable.printStackTrace(new PrintWriter(writer));
        mensagemSaida = writer.toString();
    }
}
