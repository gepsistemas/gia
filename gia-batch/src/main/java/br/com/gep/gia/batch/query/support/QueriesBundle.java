package br.com.gep.gia.batch.query.support;

import com.opengamma.elsql.ElSqlBundle;
import com.opengamma.elsql.ElSqlConfig;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class QueriesBundle {

    private static final String TOKEN = "@schema@";
    private static final ElSqlBundle elSqlBundle;
    private static final ThreadLocal<String> schemaThreadLocal;

    static {
        elSqlBundle = ElSqlBundle.of(ElSqlConfig.DEFAULT, QueriesBundle.class);
        schemaThreadLocal = new ThreadLocal<>();
    }

    public static void setSchema(String schema) {
        schemaThreadLocal.set(schema);
    }

    public static String getSchema() {
        return schemaThreadLocal.get();
    }

    public static String getSql(String name, SqlParameterSource params) {
        return injectSchema(elSqlBundle.getSql(name, params));
    }

    public static String getSql(String name) {
        return injectSchema(elSqlBundle.getSql(name));
    }

    private static String injectSchema(String sql) {
        if (sql == null) return null;

        String schema = schemaThreadLocal.get();
        if (schema == null) schema = "";
        if (!"".equals(schema)) schema += ".";

        return sql.replaceAll(TOKEN, schema);
    }
}
