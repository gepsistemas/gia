package br.com.gep.gia.batch.step;

import br.com.gep.gia.batch.reader.ItemReader;
import br.com.gep.gia.marshaller.registros.Estrutura;

import java.beans.Statement;
import java.util.Map;

public class SimpleStep<T> implements Step {

    protected ItemReader<T> reader;

    public ItemReader<T> getReader() {
        return reader;
    }

    public void setReader(ItemReader<T> reader) {
        this.reader = reader;
    }

    @Override
    public void execute(Estrutura context, Map parameters) throws Exception {
        T value = reader.read();
        if (value == null) return;

        String simpleName = value.getClass().getSimpleName();
        Object target = simpleName.startsWith("Quadro") ? context.getPrincipal() : context;
        String setter = "set" + simpleName;
        new Statement(target, setter, new Object[] { value }).execute();
    }
}
