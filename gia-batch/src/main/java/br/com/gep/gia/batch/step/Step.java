package br.com.gep.gia.batch.step;

import br.com.gep.gia.marshaller.registros.Estrutura;

import java.util.Map;

public interface Step {

    void execute(Estrutura context, Map parameters) throws Exception;
}
