package br.com.gep.gia.batch.execution.jdbc;

import br.com.gep.gia.batch.execution.ExecucaoGia;
import br.com.gep.gia.batch.execution.ExecucaoGiaDao;
import br.com.gep.gia.batch.execution.StatusExecucao;
import com.opengamma.elsql.ElSqlBundle;
import com.opengamma.elsql.ElSqlConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class JdbcExecucaoGiaDao implements ExecucaoGiaDao {

    private static final Log logger = LogFactory.getLog(JdbcExecucaoGiaDao.class);

    public static final String TABELA = "batch_gia";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private SimpleJdbcInsert jdbcInsert;
    private ElSqlBundle elSqlBundle;

    public JdbcExecucaoGiaDao(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName(TABELA)
                .usingGeneratedKeyColumns("id");

        elSqlBundle = ElSqlBundle.of(ElSqlConfig.DEFAULT, getClass());
    }

    @Override
    public ExecucaoGia buscar(long id) {
        try {
            String sql = elSqlBundle.getSql("BuscarPorId");
            SqlParameterSource param = new MapSqlParameterSource("id", id);

            return namedParameterJdbcTemplate.queryForObject(sql, param,  new ExecucaoGiaRowMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public ExecucaoGia buscar(String cnpjEstabelecimento, int ano, int mes) {
        try {
            String sql = elSqlBundle.getSql("BuscarPorEstabelecimentoAnoMes");
            SqlParameterSource params = new MapSqlParameterSource()
                    .addValue("cnpj", cnpjEstabelecimento)
                    .addValue("ano", ano)
                    .addValue("mes", mes);

            return namedParameterJdbcTemplate.queryForObject(sql, params, new ExecucaoGiaRowMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<ExecucaoGia> listarPorCliente(String cnpjCliente) {
        String sql = elSqlBundle.getSql("ListarPorCliente");
        SqlParameterSource param = new MapSqlParameterSource("cnpj", cnpjCliente);
        return namedParameterJdbcTemplate.query(sql, param, new ExecucaoGiaRowMapper());
    }

    @Override
    public List<ExecucaoGia> listarPorEstabelecimento(String cnpjEstabelecimento) {
        String sql = elSqlBundle.getSql("ListarPorEstabelecimento");
        SqlParameterSource param = new MapSqlParameterSource("cnpj", cnpjEstabelecimento);
        return namedParameterJdbcTemplate.query(sql, param, new ExecucaoGiaRowMapper());
    }

    @Override
    public void criar(ExecucaoGia execucao) {
        SqlParameterSource params = new BeanPropertySqlParameterSource(execucao);

        Number id = jdbcInsert.executeAndReturnKey(params);
        if (id != null) execucao.setId(id.longValue());
    }

    @Override
    public void atualizar(ExecucaoGia execucao) {
        String sql = elSqlBundle.getSql("Atualizar");
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("inicio", execucao.getInicio())
                .addValue("termino", execucao.getTermino())
                .addValue("status", execucao.getStatus().toString())
                .addValue("mensagemSaida", truncarMensagemSaida(execucao.getMensagemSaida()))
                .addValue("id", execucao.getId());

        namedParameterJdbcTemplate.update(sql, params);
    }

    private String truncarMensagemSaida(String mensagemSaida) {
        int tamanhoMaximo = 2500;
        if (mensagemSaida != null && mensagemSaida.length() > tamanhoMaximo) {
            logger.debug("Truncando mensagem longa antes de atualizar ExecucaoGia, a mensagem original é: " + mensagemSaida);
            return mensagemSaida.substring(0, tamanhoMaximo);
        } else {
            return mensagemSaida;
        }
    }

    private class ExecucaoGiaRowMapper implements RowMapper<ExecucaoGia> {

        @Override
        public ExecucaoGia mapRow(ResultSet rs, int rowNum) throws SQLException {
            ExecucaoGia execucao = new ExecucaoGia();
            execucao.setId(rs.getLong("id"));
            execucao.setCnpjCliente(rs.getString("cnpj_cliente"));
            execucao.setCnpjEstabelecimento(rs.getString("cnpj_estabelecimento"));
            execucao.setNome(rs.getString("nome"));
            execucao.setAno(rs.getInt("ano"));
            execucao.setMes(rs.getInt("mes"));
            execucao.setArquivo(rs.getString("arquivo"));
            execucao.setInicio(rs.getTimestamp("inicio"));
            execucao.setTermino(rs.getTimestamp("termino"));
            execucao.setStatus(StatusExecucao.valueOf(rs.getString("status")));
            execucao.setMensagemSaida(rs.getString("mensagem_saida"));
            return execucao;
        }
    }
}
