package br.com.gep.gia.batch.reader.support;

import br.com.gep.gia.batch.GiaConfig;
import br.com.gep.gia.batch.reader.AnexoItemReader;
import br.com.gep.gia.batch.reader.ItemReader;
import br.com.gep.gia.batch.reader.JdbcItemReader;
import br.com.gep.gia.batch.reader.NoOpItemReader;
import br.com.gep.gia.batch.query.support.QueriesBundle;
import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.registros.detalhes.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ItemReaderFactory {

    private DataSource dataSource;
    private static ThreadLocal<Map<String, Object>> params;

    private final Map<Class<?>, Class<?>> anexoDetalheMap;

    public ItemReaderFactory(DataSource dataSource) {
        Assert.notNull(dataSource, "dataSource nao deve ser nulo");

        this.dataSource = dataSource;
        params = new ThreadLocal<>();

        anexoDetalheMap = new HashMap<>();
        anexoDetalheMap.put(Anexo01.class, DetalheAnexo01.class);
        anexoDetalheMap.put(Anexo01C.class, DetalheAnexo01Ce05C.class);
        anexoDetalheMap.put(Anexo02.class, DetalheAnexo02e06.class);
        anexoDetalheMap.put(Anexo05.class, DetalheAnexo05.class);
        anexoDetalheMap.put(Anexo05A.class, DetalheAnexo05AB.class);
        anexoDetalheMap.put(Anexo05B.class, DetalheAnexo05AB.class);
        anexoDetalheMap.put(Anexo05C.class, DetalheAnexo01Ce05C.class);
        anexoDetalheMap.put(Anexo06.class, DetalheAnexo02e06.class);
        anexoDetalheMap.put(Anexo07A.class, DetalheAnexo07AB.class);
        anexoDetalheMap.put(Anexo07B.class, DetalheAnexo07AB.class);
        anexoDetalheMap.put(Anexo08.class, DetalheAnexo08e10.class);
        anexoDetalheMap.put(Anexo09.class, DetalheAnexo09.class);
        anexoDetalheMap.put(Anexo10.class, DetalheAnexo08e10.class);
        anexoDetalheMap.put(Anexo14.class, DetalheAnexo14e15.class);
        anexoDetalheMap.put(Anexo15.class, DetalheAnexo14e15.class);
        anexoDetalheMap.put(Anexo16.class, DetalheAnexo16.class);
    }

    public void setParams(Map<String, Object> params) {
        this.params.set(params);
    }

    @SuppressWarnings("unchecked")
    public <T> ItemReader<T> create(Class<T> klass) {
        String simpleName = klass.getSimpleName();
        boolean isAnexoComDetalhe = anexoDetalheMap.containsKey(klass);
        ItemReader<T> reader;

        try {
            Class<?> readerClass = Class.forName("br.com.gep.gia.batch.reader." + simpleName + "ItemReader");
            reader = (ItemReader<T>) readerClass.newInstance();
        } catch (ClassNotFoundException e) {
            if (isAnexoComDetalhe)
                reader = (ItemReader<T>) new AnexoItemReader<>((Class<? extends AnexoComDetalhe>) klass, anexoDetalheMap.get(klass));
            else
                reader = new JdbcItemReader<>();
        } catch (InstantiationException | IllegalAccessException e) {
            return new NoOpItemReader();
        }

        if (isAnexoComDetalhe) simpleName = "Detalhe" + simpleName;

        setProperties(klass, simpleName, reader);

        return reader;
    }

    @SuppressWarnings("unchecked")
    private <T> void setProperties(Class<T> klass, String queryName, ItemReader<T> reader) {
        if (reader instanceof JdbcItemReader) {
            ((JdbcItemReader) reader).setDataSource(dataSource);

            GiaConfig config = (GiaConfig) params.get().get("config");

            MapSqlParameterSource sqlParams = new MapSqlParameterSource(params.get());
            sqlParams.addValue("anexo01C", config != null && config.isAnexo01CIncluso());
            sqlParams.addValue("anexo05C", config != null && config.isAnexo05CIncluso());

            ((JdbcItemReader) reader).setSql(QueriesBundle.getSql(queryName, sqlParams));
            ((JdbcItemReader) reader).setParams(sqlParams);
            ((JdbcItemReader) reader).setRowMapper(new BeanPropertyRowMapper<>(klass));
        }
    }
}
