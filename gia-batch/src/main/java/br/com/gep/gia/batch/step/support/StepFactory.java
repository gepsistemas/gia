package br.com.gep.gia.batch.step.support;

import br.com.gep.gia.batch.reader.ItemReader;
import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import br.com.gep.gia.batch.step.SimpleStep;
import br.com.gep.gia.batch.step.Step;

public class StepFactory {

    private ItemReaderFactory readerFactory;

    public void setReaderFactory(ItemReaderFactory readerFactory) {
        this.readerFactory = readerFactory;
    }

    public <T> Step create(Class<T> klass) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ItemReader<T> reader = readerFactory.create(klass);

        SimpleStep<T> step = new SimpleStep<>();
        step.setReader(reader);

        return step;
    }
}
