package br.com.gep.gia.batch.reader;

import br.com.gep.gia.marshaller.registros.Anexo10;
import br.com.gep.gia.marshaller.registros.detalhes.DetalheAnexo08e10;

public class Anexo10ItemReader extends AnexoItemReader<Anexo10, DetalheAnexo08e10> {

    public Anexo10ItemReader() {
        super(Anexo10.class, DetalheAnexo08e10.class);
    }

    @Override
    public Anexo10 read() throws Exception {
        Anexo10 anexo10 = super.read();

        boolean temValores = anexo10.getTotalGeral() > 0;

        return temValores ? anexo10 : null;
    }
}
