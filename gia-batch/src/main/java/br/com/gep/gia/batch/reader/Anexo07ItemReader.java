package br.com.gep.gia.batch.reader;

import br.com.gep.gia.marshaller.registros.Anexo07;

public class Anexo07ItemReader extends JdbcItemReader<Anexo07> {

    @Override
    public Anexo07 read() throws Exception {
        Anexo07 anexo07 = super.read();

        boolean temValores = (anexo07.getTotalCreditos() + anexo07.getTotalDebitos()) != 0;

        return temValores ? anexo07 : null;
    }
}
