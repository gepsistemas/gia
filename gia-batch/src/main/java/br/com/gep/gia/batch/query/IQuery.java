package br.com.gep.gia.batch.query;

public interface IQuery<T> {

    T execute(Object... parameters);
}
