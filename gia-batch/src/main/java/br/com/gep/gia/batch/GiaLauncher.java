package br.com.gep.gia.batch;

import br.com.gep.gia.batch.execution.ExecucaoGia;
import br.com.gep.gia.batch.execution.ExecucaoGiaDao;
import br.com.gep.gia.batch.execution.StatusExecucao;
import br.com.gep.gia.batch.execution.jdbc.JdbcExecucaoGiaDao;
import br.com.gep.gia.batch.query.CabecalhoQuery;
import br.com.gep.gia.batch.query.ClienteQuery;
import br.com.gep.gia.batch.query.TabelasAuxiliares;
import br.com.gep.gia.batch.reader.support.ItemReaderFactory;
import br.com.gep.gia.batch.query.support.QueriesBundle;
import br.com.gep.gia.batch.step.*;
import br.com.gep.gia.batch.step.support.StepFactory;
import br.com.gep.gia.marshaller.registros.*;
import br.com.gep.gia.marshaller.registros.detalhes.QuadroB;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class GiaLauncher {

    private static final Log logger = LogFactory.getLog(GiaLauncher.class);

    private final ItemReaderFactory readerFactory;
    private final StepFactory stepFactory;
    private final CabecalhoQuery cabecalhoQuery;
    private final ClienteQuery clienteQuery;
    private final ExecucaoGiaDao execucaoDao;
    private final TabelasAuxiliares tabelasAuxiliares;
    private String schema;
    private String destinationDir = "";
    private TaskExecutor taskExecutor;

    public GiaLauncher(DataSource dataSource) {
        Assert.notNull(dataSource, "dataSource não deve ser nulo");

        readerFactory = new ItemReaderFactory(dataSource);
        stepFactory = new StepFactory();
        stepFactory.setReaderFactory(readerFactory);
        cabecalhoQuery = new CabecalhoQuery();
        cabecalhoQuery.setDataSource(dataSource);
        clienteQuery = new ClienteQuery();
        clienteQuery.setDataSource(dataSource);
        execucaoDao = new JdbcExecucaoGiaDao(dataSource);
        tabelasAuxiliares = new TabelasAuxiliares(dataSource);
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public void setDestinationDir(String destinationDir) {
        Assert.hasText(destinationDir, "destinationDir inválido");
        Assert.state(Files.isDirectory(Paths.get(destinationDir)), "destinationDir não é um diretório válido");

        this.destinationDir = destinationDir;
    }

    private TaskExecutor getTaskExecutor() {
        if (taskExecutor == null) {
            logger.info("taskExecutor nao foi setado. Usando SyncTaskExecutor.");
            taskExecutor = new SyncTaskExecutor();
        }

        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public ExecucaoGia run(String cnpj, GiaConfig config, String outputFile) throws Exception {
        Assert.hasText(cnpj, "CNPJ inválido");
        Assert.notNull(config, "config não deve ser nulo");

        QueriesBundle.setSchema(schema);
        final Cabecalho cabecalho = cabecalhoQuery.execute(cnpj);
        if (cabecalho == null) return null;

        boolean incluirAnexoC = cabecalho.getAno() >= 2014 && cabecalho.getMes() >= 9;
        config.setAnexo01CIncluso(incluirAnexoC);
        config.setAnexo05CIncluso(incluirAnexoC);

        tabelasAuxiliares.criar(incluirAnexoC, incluirAnexoC);

        final Map<String, Object> params = new HashMap<>();
        params.put("cnpj", cnpj);
        params.put("config", config);

        if (outputFile == null || "".equals(outputFile)) {
            outputFile = String.format("GIA_RS_%s_%d_%02d.txt",
                    cnpj, cabecalho.getAno(), cabecalho.getMes());
            outputFile = new File(destinationDir, outputFile).getAbsolutePath();

            logger.info("outputFile não informado. Arquivo será salvo em :" + outputFile);
        }
        params.put("outputFile", outputFile);

        ExecucaoGia novaExecucao = getNextExecution(cnpj, outputFile, cabecalho);

        executeSteps(cabecalho, params, novaExecucao);

        return novaExecucao;
    }

    private ExecucaoGia getNextExecution(String cnpjEstabelecimento, String outputFile, Cabecalho cabecalho)
            throws ExecucaoJaFoiIniciadaException, ExecucaoJaEstaCompletaException {
        ExecucaoGia ultimaExecucao = execucaoDao.buscar(cnpjEstabelecimento, cabecalho.getAno(), cabecalho.getMes());
        if (ultimaExecucao != null) {
            StatusExecucao status = ultimaExecucao.getStatus();
            if (status == StatusExecucao.INICIADO) {
                throw new ExecucaoJaFoiIniciadaException();
            }
            if (status == StatusExecucao.COMPLETO) {
                throw new ExecucaoJaEstaCompletaException();
            }

            return ultimaExecucao;
        }

        ExecucaoGia novaExecucao = new ExecucaoGia();
        String cnpjCliente = clienteQuery.execute();
        novaExecucao.setCnpjCliente(cnpjCliente);
        novaExecucao.setCnpjEstabelecimento(cnpjEstabelecimento);
        novaExecucao.setAno(cabecalho.getAno());
        novaExecucao.setMes(cabecalho.getMes());
        novaExecucao.setArquivo(outputFile);
        execucaoDao.criar(novaExecucao);

        return novaExecucao;
    }

    private void executeSteps(final Cabecalho cabecalho, final Map<String, Object> params, final ExecucaoGia execucao) {
        try {
            getTaskExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        QueriesBundle.setSchema(schema);
                        readerFactory.setParams(params);

                        Estrutura estrutura = new Estrutura(cabecalho);

                        execucao.setInicio(new Date());
                        execucao.setStatus(StatusExecucao.INICIADO);
                        execucaoDao.atualizar(execucao);

                        logger.info(String.format(
                                "Iniciando execução da GIA-RS para o CNPJ [%s] e competência [%d/%02d]",
                                execucao.getCnpjCliente(), execucao.getAno(), execucao.getMes()));

                        List<Step> steps = buildSteps();
                        for (Step step : steps) {
                            step.execute(estrutura, params);
                        }

                        execucao.setStatus(StatusExecucao.COMPLETO);
                    }
                    catch (Throwable t) {
                        execucao.setStatus(StatusExecucao.FALHOU);
                        execucao.setMensagemSaida(t);

                        logger.info(String.format(
                                "Execução da GIA-RS para o CNPJ [%s] e competência [%d/%02d] falhou",
                                execucao.getCnpjCliente(), execucao.getAno(), execucao.getMes()));

                        if (t instanceof RuntimeException) {
                            throw (RuntimeException) t;
                        }
                        else if (t instanceof Error) {
                            throw (Error) t;
                        }
                        throw new IllegalStateException(t);
                    }
                    finally {
                        execucao.setTermino(new Date());
                        execucaoDao.atualizar(execucao);

                        logger.info(String.format(
                                "Execução da GIA-RS para o CNPJ [%s] e competência [%d/%02d] finalizada com status [%s]",
                                execucao.getCnpjCliente(), execucao.getAno(), execucao.getMes(), execucao.getStatus()));
                    }
                }
            });
        }
        catch (TaskRejectedException e) {
            execucao.setStatus(StatusExecucao.FALHOU);
            execucao.setMensagemSaida(e);
            execucaoDao.atualizar(execucao);
        }
    }

    public ExecucaoGia run(String cnpj, GiaConfig config) throws Exception {
        return run(cnpj, config, null);
    }

    public List<Step> buildSteps() throws Exception {
        List<Step> steps = new LinkedList<>();
        steps.add(stepFactory.create(Contribuinte.class));
        steps.add(new ConfigStep());
        steps.add(stepFactory.create(QuadroB.class));
        steps.add(stepFactory.create(Anexo01.class));
        steps.add(stepFactory.create(Anexo01C.class));
        steps.add(stepFactory.create(Anexo02.class));
        steps.add(stepFactory.create(Anexo05.class));
        steps.add(stepFactory.create(Anexo05A.class));
        steps.add(stepFactory.create(Anexo05B.class));
        steps.add(stepFactory.create(Anexo05C.class));
        steps.add(stepFactory.create(Anexo06.class));
        steps.add(stepFactory.create(Anexo07.class));
        steps.add(stepFactory.create(Anexo07A.class));
        steps.add(stepFactory.create(Anexo07B.class));
        steps.add(stepFactory.create(Anexo14.class));
        steps.add(stepFactory.create(Anexo10.class));
        steps.add(stepFactory.create(Anexo15.class));
        steps.add(new ObservacoesStep());
        steps.add(new AjustesFinaisStep());
        steps.add(new WriteFileStep());

        return steps;
    }
}
