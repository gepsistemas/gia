package br.com.gep.gia.batch.execution;

public enum StatusExecucao {
    INICIADO, COMPLETO, FALHOU;
}
