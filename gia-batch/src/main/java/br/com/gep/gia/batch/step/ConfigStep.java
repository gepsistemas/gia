package br.com.gep.gia.batch.step;

import br.com.gep.gia.batch.GiaConfig;
import br.com.gep.gia.marshaller.registros.Estrutura;
import br.com.gep.gia.marshaller.registros.Principal;
import br.com.gep.gia.marshaller.registros.detalhes.QuadroC;

import java.util.Map;

public class ConfigStep implements Step {

    @Override
    public void execute(Estrutura context, Map parameters) {
        GiaConfig config = (GiaConfig) parameters.get("config");

        Principal principal = context.getPrincipal();
        principal.setRetificacao(config.isRetificadora());

        QuadroC quadroC = principal.getQuadroC();

        quadroC.setNumeroEmpregados(config.getNumeroEmpregados());
        quadroC.setValorFolhaPagamento(config.getValorFolhaPagamento());

        if (config.getQuadroE() != null)
            principal.setQuadroE(config.getQuadroE());

        principal.getTotais().setInicioAtividade(config.isInicioAtividade());
        principal.getTotais().setFimAtividade(config.isFimAtividade());
    }
}
