package br.com.gep.gia.batch.execution;

import java.util.List;

public interface ExecucaoGiaDao {

    ExecucaoGia buscar(long id);

    ExecucaoGia buscar(String cnpjEstabelecimento, int ano, int mes);

    List<ExecucaoGia> listarPorCliente(String cnpjCliente);

    List<ExecucaoGia> listarPorEstabelecimento(String cnpjEstabelecimento);

    void criar(ExecucaoGia execucao);

    void atualizar(ExecucaoGia execucaoGia);
}
