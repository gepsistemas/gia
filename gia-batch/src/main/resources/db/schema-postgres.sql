create table batch_gia (
  id serial not null primary key,
  cnpj_cliente varchar(14) not null,
  cnpj_estabelecimento varchar(14) not null,
  nome varchar(100),
  ano integer NOT NULL,
  mes integer NOT NULL,
  arquivo varchar(255),
  inicio timestamp,
  termino timestamp,
  status varchar(10),
  mensagem_saida varchar(2500)
);